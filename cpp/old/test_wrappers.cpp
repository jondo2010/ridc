#include <stdio.h>
#include <stdlib.h>

#include "ridc.h"

using namespace std;

/*
class ImplicitOde : public ODE
{
public:
    ImplicitOde(int my_neq, int my_nt, double my_ti, double my_tf, double my_dt)
    {
        neq = my_neq;
        nt = my_nt;
        ti = my_ti;
        tf = my_tf;
        dt = my_dt;
    }

    /// user implemented rhs function, u'=rhs(t,u)
    void rhs(double t, const std::vector<double> &u, std::vector<double> &f)
override
    {
        for (int i = 0; i < neq; i++)
        {
            f[i] = -(i + 1) * t * u[i];
        }
    }

    void step(double t, const std::vector<double> &u, std::vector<double> &unew)
override
    {
        auto fold = std::vector<double>(neq);
        rhs(t, u, fold);

        // backward Euler update (simplistic is ODE is linear
        for (int i = 0; i < neq; i++)
        {
            unew[i] = (1.0) / (1 + dt * (t + dt) * (i + 1)) * u[i];
        }
    }
};
*/

namespace {
template <size_t NEQ> void rhs(const double t, const double *u, double *f) {
  for (size_t i = 0; i < NEQ; i++) {
    f[i] = -(i + 1) * t * u[i];
  }
}

template <size_t NEQ>
void step_forward(const double t, const double dt, const double *u,
                  double *u_new) {
  auto f_old = std::vector<double>(NEQ);
  rhs<NEQ>(t, u, f_old.data());
  for (size_t i = 0; i < NEQ; i++) {
    u_new[i] = u[i] + dt * f_old[i];
  }
}
} // namespace

/*
class ExplicitOde : public ODE
{
public:
    ExplicitOde(int my_neq, int my_nt, double my_ti, double my_tf, double my_dt)
    {
        neq = my_neq;
        nt = my_nt;
        ti = my_ti;
        tf = my_tf;
        dt = my_dt;
    }

    // User implemented rhs function, u'=rhs(t,u)
    void rhs(double t, const std::vector<double> &u, std::vector<double> &f)
override
    {
        for (int i = 0; i < neq; i++)
        {
            f[i] = -(i + 1) * t * u[i];
        }
    }

    void step(double t, const std::vector<double> &u, std::vector<double>
&u_new) override
    {
        auto f_old = std::vector<double>(neq);
        rhs(t, u, f_old);

        for (int i = 0; i < neq; i++)
        {
            u_new[i] = u[i] + dt * f_old[i];
        }
    }
};
*/

extern "C" {
/*
void main_implicit(int order, int nt, int neq, double *sol)
{
    //int neq = 2;
    int ti = 0;
    int tf = 1;
    double dt = (double)(tf - ti) / nt; // compute dt

    // initialize ODE variable
    ImplicitOde *ode = new ImplicitOde(neq, nt, ti, tf, dt);

    // specify initial condition
    for (int i = 0; i < neq; i++)
    {
        sol[i] = 1.0;
    }

    // call ridc
    ridc_be(ode, order, sol);

    delete ode;
}
*/


void main_explicit(const size_t order, const size_t nt, double *sol) {
  int ti = 0;
  int tf = 1;
  double dt = (double)(tf - ti) / nt; // compute dt
  const size_t NEQ = 2;

  auto callbacks = OdeCallbacks{::rhs<NEQ>, ::step_forward<NEQ>};

  // specify initial condition
  for (size_t i = 0; i < NEQ; i++) {
    sol[i] = 1.0;
  }

  // call ridc
  ridc_fe(callbacks, dt, ti, tf, NEQ, nt, order, sol);
}
}