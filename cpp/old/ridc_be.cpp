#include "ridc.h"

#include <stdio.h>

#include <iostream>
#include <vector>

using namespace std;

///////////////////////////////////////////////////////
// Function Name: ridc_be
// Usage: main ridc loop
//
///////////////////////////////////////////////////////
// Assumptions: none
//
///////////////////////////////////////////////////////
// Inputs:
//	   int order, order of integrator
//	   ode, ode definition + problem parameters
//         sol, initial condition
//
///////////////////////////////////////////////////////
// Outputs: (by reference)
//	    sol, solution at finest level, final time
//
///////////////////////////////////////////////////////
// Functions Called: corr_be, integration matrices,
//           initial condition
//
///////////////////////////////////////////////////////
// Called By: none
//
///////////////////////////////////////////////////////
void ridc_be(ODE *ode, int order, double *sol)
{
    // unpack param
    double dt = ode->dt;
    double ti = ode->ti;
    int neq = ode->neq;
    int Nt = ode->nt;

    // setup memory stencil for RIDC
    std::vector<int> stencilSize(order);
    for (int p = 0; p < (order - 1); p++)
    {
        stencilSize[p] = p + 2;
    }
    stencilSize[order - 1] = 1;

    // memory footprint for quadrature approximations, for all levels
    std::vector<std::vector<std::vector<double>>> u(order);
    std::vector<std::vector<std::vector<double>>> f(order);
    for (int p = 0; p < order; p++)
    {
        u[p] = std::vector<std::vector<double>>(stencilSize[p], std::vector<double>(neq));
        f[p] = std::vector<std::vector<double>>(stencilSize[p], std::vector<double>(neq));
    }

    // this is a temporary variable for storing the solution at the new
    // time step (for each level)
    auto unew = std::vector<std::vector<double>>(order, std::vector<double>(neq));
    auto fnew = std::vector<std::vector<double>>(order, std::vector<double>(neq));

    double ***S = new double **[order];
    for (int i = 0; i < order; i++)
    {
        S[i] = new double *[i + 1];
        for (int j = 0; j < i + 1; j++)
        {
            S[i][j] = new double[i + 1];
        }
    }

    // initialize integration matrices
    for (int j = 1; j < order; j++)
    {
        integration_matrices(j + 1, S[j]);
    }

    // get initial condition
    for (int i = 0; i < neq; i++)
    {
        u[0][0][i] = sol[i];
    }

    // compute f[0][0]
    ode->rhs(ti, u[0][0], f[0][0]);

    // copy initial condition for each RIDC level
    for (int j = 1; j < order; j++)
    {
        // copy initial condition for each RIDC level
        for (int j = 1; j < order; j++)
        {
            for (int i = 0; i < neq; i++)
            {
                u[j][0][i] = u[0][0][i];
                f[j][0][i] = f[0][0][i];
            }
        }
    }

    // flag for whether to shift memory in stencil
    auto doMemShift = std::vector<bool>(order);

    // flag for whether the node is able to compute the next step for
    // each correction level
    auto doComputeStep = std::vector<bool>(order);

    // counter for where the node is for each correction level
    auto ii = std::vector<int>(order, 0);

    ///////////////////////////////////////////////////////////////////
    // START-UP ROUTINES
    ///////////////////////////////////////////////////////////////////

    // after startnum, all the nodes should be computing
    int startnum = (order * (order + 1) / 2 - 1);

    // special case
    if (order == 1)
    {
        startnum = 1;
    }

    auto filter = std::vector<std::vector<double>>(order, std::vector<double>(startnum, 0));

    if (order == 1)
    {
        filter[0][1] = 1;
    }
    else
    {
        int q = 0;

        for (int p = 1; p < order; p++)
        {
            // all except level p step
            for (int pp = 0; pp < p; pp++)
            {
                filter[pp][q] = 1;
            }

            q++;

            // one step
            for (int count = 0; count < p - 1; count++)
            {
                filter[p][q] = 1;
                q++;
            }

            // all step, including level p
            for (int pp = 0; pp <= p; pp++)
            {
                filter[pp][q] = 1;
            }
            q++;
        } // loop over order
    }     // create filter

    ///////////////////////////////////////////////////////////////////
    // MAIN TIME LOOP
    ///////////////////////////////////////////////////////////////////

    int i = 0; // counter
    while (1)
    {
        std::cout << "-- i = " << i << " --\n";
        {
            ///////////////////////////////////////////////////////////////////
            // PIPELINE UPDATE
            ///////////////////////////////////////////////////////////////////
#pragma omp parallel for
            for (int p = 0; p < order; p++)
            {
                doComputeStep[p] = false;
                doMemShift[p] = false;

                if ((ii[p] < Nt) &&
                    ((i >= (startnum - 1)) || (filter[p][i] == 1)))
                {
                    doComputeStep[p] = true;
                }

                if (doComputeStep[p])
                {
                    // Now do a step of the pth Corrector/Predictor
                    int index_into_array = min(ii[p], stencilSize[p] - 1);
                    // compute current time
                    double t = ti + (ii[p]) * dt;
                    if (p == 0)
                    {
                        // prediction step
                        ode->step(t, u[0][index_into_array], unew[0]);

                        // populate memory stencil for f
                        ode->rhs(t + dt, unew[0], fnew[0]);
                    }
                    else
                    {
                        int index_sth = min(ii[p], p - 1);

                        //correction step!
                        corr_be(ode, u[p][index_into_array], f[p - 1], S[p],
                                index_sth, p + 1, t, unew[p]);

                        // populate memory stencil if not the last correction loop
                        if (p < (order - 1))
                        {
                            ode->rhs(t + dt, unew[p], fnew[p]);
                        }
                    }
                    ii[p]++;

                    if (ii[p] >= stencilSize[p])
                    {
                        doMemShift[p] = true;
                    }
                }   // end if doComputeStep[p]
            }       // end p
#pragma omp barrier // ** BARRIER 1 **
        }           // end pragma for loop

        // memshifts
        for (int p = 0; p < order; p++)
        {
            if (doComputeStep[p])
            {
                if (!(doMemShift[p]))
                {
                    int index_into_array = ii[p];
                    //double *temp;

                    // Swap u[p][index_into_array] with unew[p]
                    auto temp = u[p][index_into_array];
                    u[p][index_into_array] = unew[p];
                    unew[p] = temp;

                    // Swap f[p][index_into_array] with fnew[p]
                    temp = f[p][index_into_array];
                    f[p][index_into_array] = fnew[p];
                    fnew[p] = temp;
                }
                else
                {
                    //double *temp;
                    auto temp = u[p][0];
                    for (int k = 0; k < (stencilSize[p] - 1); k++)
                    {
                        u[p][k] = u[p][k + 1];
                    }
                    u[p][stencilSize[p] - 1] = unew[p];
                    unew[p] = temp;

                    temp = f[p][0];
                    for (int k = 0; k < (stencilSize[p] - 1); k++)
                    {
                        f[p][k] = f[p][k + 1];
                    }
                    f[p][stencilSize[p] - 1] = fnew[p];
                    fnew[p] = temp;
                } // end doMemShift
            }     // end doComputeStep
        }         // end p

        if (ii[order - 1] == Nt)
        {
            break;
        }
        i++;
    } // end while loop

    ///////////////////////////////////////////////////////////////////
    // END: MAIN TIME LOOP
    ///////////////////////////////////////////////////////////////////

    int p = order - 1;
    for (int i = 0; i < neq; i++)
    {
        sol[i] = u[p][stencilSize[p] - 1][i];
    }

    for (int p = 0; p < order; p++)
    {
        for (int j = 0; j < p + 1; j++)
        {
            delete[] S[p][j];
        }
        delete[] S[p];
    }
    delete[] S;

    //delete[] stencilSize;
} // end ridc_be

///////////////////////////////////////////////////////
// Function Name: corr_be
// Usage: computes one correction step
//
///////////////////////////////////////////////////////
// Assumptions: none
//
///////////////////////////////////////////////////////
// Inputs:
//	   uold, sol at prev step, current level
//	   fprev, sol' from previous level
//	   S, integration matrix
//	   index,
//	   level, which correction equation are we solving?
//	   t, time
//	   ode, ode definition + problem parameters
//
///////////////////////////////////////////////////////
// Outputs: (by reference)
//	    unew, new solution vector
//
///////////////////////////////////////////////////////
// Called By: ridc_be
//
///////////////////////////////////////////////////////
void corr_be(ODE *ode,
             std::vector<double> &uold,
             const std::vector<std::vector<double>> &fprev,
             double **S,
             int index, int level,
             double t,
             std::vector<double> &unew)
{
    std::cout << " corr_be(index="
              << index << ", level=" << level
              << ", t=" << t << ")\n";

    int neq = ode->neq;
    double dt = ode->dt;

    // backwards euler update
    for (int j = 0; j < neq; j++)
    {
        uold[j] -= dt * (fprev[index + 1][j]);
        for (int i = 0; i < level; i++)
        {
            uold[j] += dt * (S[index + 1][i] - S[index][i]) * fprev[i][j] * (level - 1);
        }
    }

    ode->step(t, uold, unew);

} // end corr_be
