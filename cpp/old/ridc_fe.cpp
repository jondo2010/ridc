#include "ridc.h"

#include <stdio.h>

#include <array>
#include <iostream>
#include <vector>

using namespace std;

struct RidcRuntime
{
  std::vector<size_t> stencilSize;
  // variable containing flag for whether to shift memory in stencil
  std::vector<bool> doMemShift;
  // flag for whether the node is able to compute the next step for
  // each correction level
  std::vector<bool> doComputeStep;
  // after startnum, all the nodes should be computing
  int startnum;

  std::vector<std::vector<bool>>
      filter; //(order, std::vector<double>(startnum, 0));
};

auto filter_init(const int order, const int startnum)
{
  std::vector<std::vector<bool>> filter(order, std::vector<bool>(startnum, 0));
  if (order == 1)
  {
    filter[0][0] = true;
  }
  else
  {
    int q = 0;

    for (int p = 1; p < order; p++)
    {
      // all except level p step
      for (int pp = 0; pp < p; pp++)
      {
        filter[pp][q] = true;
      }

      q++;

      // one step
      for (int count = 0; count < p - 1; count++)
      {
        filter[p][q] = true;
        q++;
      }

      // all step, including level p
      for (int pp = 0; pp <= p; pp++)
      {
        filter[pp][q] = true;
      }
      q++;
    } // loop over order
  }   // create filter

  return filter;
}

auto stencilSize_init(const int order)
{
  std::vector<size_t> stencilSize(order);
  // setup memory stencil for RIDC
  for (int p = 0; p < (order - 1); p++)
  {
    stencilSize[p] = p + 2;
  }
  stencilSize[order - 1] = 1;
  return stencilSize;
}

constexpr auto startnum_init(const int order)
{
  return order == 1 ? 1 : (order * (order + 1) / 2 - 1);
}

auto init(const int order)
{
  auto startnum = startnum_init(order);
  RidcRuntime rt{
      .stencilSize = stencilSize_init(order),
      .doMemShift = std::vector<bool>(order),
      .doComputeStep = std::vector<bool>(order),
      .startnum = startnum,
      .filter = filter_init(order, startnum),
  };
  return rt;
}

///////////////////////////////////////////////////////
// Function Name: ridc_fe
// Usage: main explicit ridc loop
//
///////////////////////////////////////////////////////
// Assumptions: none
//
///////////////////////////////////////////////////////
// Inputs:
//	   int order, order of integrator
//	   ode, ode definition + problem parameters
//         sol, initial condition
//
///////////////////////////////////////////////////////
// Outputs: (by reference)
//	    sol, solution at finest level, final time

void ridc_fe(const OdeCallbacks &callbacks, const double dt, const double ti,
             const double tf, const size_t neq, const size_t Nt, size_t order,
             double *sol)
{
  auto rt = init(order);

  // memory footprint for quadrature approximations, for all levels
  // [order][stencilSize][neq]
  double ***u = new double **[order];
  double ***f = new double **[order];
  for (size_t p = 0; p < order; p++)
  {
    u[p] = new double *[rt.stencilSize[p]];
    f[p] = new double *[rt.stencilSize[p]];
    for (size_t i = 0; i < rt.stencilSize[p]; i++)
    {
      u[p][i] = new double[neq];
      f[p][i] = new double[neq];
    }
  }

  // temp variable for storing sol at new time step (each level)
  double **unew = new double *[order];
  double **fnew = new double *[order];
  for (size_t j = 0; j < order; j++)
  {
    unew[j] = new double[neq];
    fnew[j] = new double[neq];
  }

  // setting variables for integration matrices
  auto S = std::vector<MatT>(order);

  // initialize integration matrices
  for (size_t j = 1; j < order; j++)
  {
    S[j] = integration_matrices(j + 1);
  }

  // get initial condition
  for (size_t i = 0; i < neq; i++)
  {
    u[0][0][i] = sol[i];
  }

  // compute f[0][0]
  callbacks.rhs(ti, u[0][0], f[0][0]);

  // copy initial condition for each RIDC level
  for (size_t j = 1; j < order; j++)
  {
    for (size_t i = 0; i < neq; i++)
    {
      u[j][0][i] = u[0][0][i];
      f[j][0][i] = f[0][0][i];
    }
  }

  // counter for where the node is for each correction level
  auto ii = std::vector<size_t>(order, 0);

  ///////////////////////////////////////////////////////////////////
  // START-UP ROUTINES
  ///////////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////////
  // MAIN TIME LOOP
  ///////////////////////////////////////////////////////////////////

  int i = 0; // counter
  while (1)
  {
    std::cout << "--- step i=" << i << " ---\n";
    {
      ///////////////////////////////////////////////////////////////////
      // PIPELINE UPDATE
      ///////////////////////////////////////////////////////////////////

//#pragma omp parallel for
      for (size_t p = 0; p < order; p++)
      {
        rt.doComputeStep[p] = false;
        rt.doMemShift[p] = false;

        if ((ii[p] < Nt) && ((i >= (rt.startnum - 1)) || rt.filter[p][i]))
        {
          rt.doComputeStep[p] = true;
        }

        if (rt.doComputeStep[p])
        {
          // Now do a step of the pth Corrector/Predictor
          const size_t ii_p = min(ii[p], rt.stencilSize[p] - 1);
          // compute current time
          const double t = ti + (ii[p]) * dt;

          if (p == 0)
          {
            std::cout << "pred unew[0] <- u[0][" << ii_p << "]\n";
            // prediction step
            callbacks.step(t, dt, u[0][ii_p], unew[0]);

            // populate memory stencil for f (needed for order > 1)
            callbacks.rhs(t + dt, unew[0], fnew[0]);
          }
          else
          {
            auto index_sth = min(ii[p], p - 1);

            // correction step!
            std::cout << "corr_fe() for unew[" << p << "]\n"
                      << " <- u[" << p << "][" << ii_p << "]\n";
            corr_fe(
                callbacks,
                neq,
                dt,
                u[p][ii_p], // soln at time level t
                const_cast<const double **>(f[p - 1]),
                S[p],
                index_sth,
                p + 1,
                t,
                const_cast<const double *>(unew[p]));

            // populate memory stencil if not the last correction loop
            if (p < (order - 1))
            {
              callbacks.rhs(t + dt, unew[p], fnew[p]);
            }
          }
          ii[p]++;

          if (ii[p] >= rt.stencilSize[p])
          {
            rt.doMemShift[p] = true;
          }
        }           // end if doComputeStep[p]
      }             // end p
//#pragma omp barrier // ** BARRIER 1 **
    }               // end pragma for loop

    // memshifts
    for (size_t p = 0; p < order; p++)
    {
      if (rt.doComputeStep[p])
      {
        if (!(rt.doMemShift[p]))
        {
          const auto index_into_array = ii[p];

          {
            auto temp = u[p][index_into_array];
            u[p][index_into_array] = unew[p];
            unew[p] = temp;
          }

          {
            auto temp = f[p][index_into_array];
            f[p][index_into_array] = fnew[p];
            fnew[p] = temp;
          }
        }
        else
        {
          {
            auto temp = u[p][0];
            for (size_t k = 0; k < (rt.stencilSize[p] - 1); k++)
            {
              u[p][k] = u[p][k + 1];
            }
            u[p][rt.stencilSize[p] - 1] = unew[p];
            unew[p] = temp;
          }

          {
            auto temp = f[p][0];
            for (size_t k = 0; k < (rt.stencilSize[p] - 1); k++)
            {
              f[p][k] = f[p][k + 1];
            }
            f[p][rt.stencilSize[p] - 1] = fnew[p];
            fnew[p] = temp;
          }
        } // end doMemShift
      }   // end doComputeStep
    }     // end p

    if (ii[order - 1] == Nt)
    {
      break;
    }
    i++;

  } // end while loop

  ///////////////////////////////////////////////////////////////////
  // END: MAIN TIME LOOP
  ///////////////////////////////////////////////////////////////////

  size_t p = order - 1;
  for (size_t i = 0; i < neq; i++)
  {
    sol[i] = u[p][rt.stencilSize[p] - 1][i];
  }
} // end ridc_fe

///////////////////////////////////////////////////////
// Function Name: corr_fe
// Usage: computes one correction step
//
///////////////////////////////////////////////////////
// Assumptions: none
//
///////////////////////////////////////////////////////
// Inputs:
//	   uold, sol at prev step, current level
//	   fprev, sol' from previous level
//	   S, integration matrix
//	   index,
//	   level, which correction equation are we solving?
//	   t, time
//	   ode, ode definition + problem parameters
//
///////////////////////////////////////////////////////
// Outputs: (by reference)
//	    unew, new solution vector
//
///////////////////////////////////////////////////////
// Called By: ridc_fe
//
///////////////////////////////////////////////////////
void corr_fe(const OdeCallbacks &callbacks, const size_t neq, const double dt,
             const double *uold,
             const double **fprev, const double **S,
             int index, int level, double t, double *unew)
{
  callbacks.step(t, dt, uold, unew);

  // std::cout << " <- f[" << level - 2 << "][" << index << "]\n";
  for (int i = 0; i < level; i++)
  {
    // std::cout << " <- f[" << level - 2 << "][" << i << "]\n";
  }

  // forward euler update
  for (size_t j = 0; j < neq; j++)
  {
    unew[j] -= dt * (fprev[index][j]);
    for (int i = 0; i < level; i++)
    {
      unew[j] = unew[j] + dt * (S[index + 1][i] - S[index][i]) * fprev[i][j] *
                              (level - 1);
    }
  }
} // end corr_fe
