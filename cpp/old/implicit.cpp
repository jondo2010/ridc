#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "ridc.h"

extern "C"
{
    void implicit_rhs_cb(double t, const double *u, double *f, const size_t neq)
    {
        for (int i = 0; i < neq; i++)
        {
            f[i] = -(i + 1) * t * u[i];
        }
    };

    void implicit_step_cb(const double t, const double dt, double *u, double *unew, const size_t neq)
    {
        double *fold = new double[neq];
        //implicit_rhs_cb(t, u, fold, neq);

        // backward Euler update (simplistic is ODE is linear
        for (int i = 0; i < neq; i++)
        {
            unew[i] = (1.0) / (1 + dt * (t + dt) * (i + 1)) * u[i];
        }
        delete[] fold;
    }

    int implicit_main(const int order, const int nt, double *sol)
    {
        int neq = 2;
        int ti = 0;
        int tf = 1;
        double dt = (double)(tf - ti) / nt; // compute dt

        // initialize ODE variable

        OdeCallbacks callbacks{
            &implicit_rhs_cb,
            &implicit_step_cb,
        };

        // specify initial condition
        for (int i = 0; i < neq; i++)
        {
            sol[i] = 1.0;
        }

        // call ridc
        ridc(&callbacks, corr_be, dt, ti, tf, neq, nt, order, sol);

        // output solution to screen
        // for (int i = 0; i < neq; i++)
        //    printf("%14.12f\n", sol[i]);

        return 0;
    }
}