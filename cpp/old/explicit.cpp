#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "ridc.h"

extern "C"
{
    void explicit_rhs_cb(double t, const double *u, double *f, const size_t neq)
    {
        for (int i = 0; i < neq; i++)
        {
            f[i] = -(i + 1) * t * u[i];
        }
    }

    void explicit_step_cb(const double t, const double dt, double *u, double *unew, const size_t neq)
    {
        double *fold = new double[neq];
        explicit_rhs_cb(t, u, fold, neq);

        for (int i = 0; i < neq; i++)
        {
            unew[i] = u[i] + dt * (fold[i]);
        }
        delete[] fold;
    }

    int explicit_main(const int order, const int nt, double *sol)
    {
        int neq = 2;
        int ti = 0;
        int tf = 1;
        double dt = (double)(tf - ti) / nt; // compute dt

        // initialize ODE variable
        OdeCallbacks callbacks{
            &explicit_rhs_cb,
            &explicit_step_cb,
        };

        // specify initial condition
        for (int i = 0; i < neq; i++)
        {
            sol[i] = 1.0;
        }

        // call ridc
        ridc(&callbacks, corr_fe, dt, ti, tf, neq, nt, order, sol);

        // output solution to screen
        // for (int i = 0; i < neq; i++)
        //    printf("%14.12f\n", sol[i]);
    }
}