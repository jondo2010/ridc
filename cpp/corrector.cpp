#include <stdio.h>

#include "ridc.h"

extern "C"
{
    void corr_fe(OdeCallbacks *ode, const size_t neq, const double dt, double *uold,
                 const double **fprev, const Matrix *S, const size_t index, const size_t level, double t,
                 double *unew)
    {
        ode->step_cb(t, dt, uold, unew, neq);

        // forward euler update
        for (size_t j = 0; j < neq; j++)
        {
            unew[j] -= dt * (fprev[index][j]);
            for (size_t i = 0; i < level; i++)
            {
                unew[j] += dt * ((*S)(index + 1, i) - (*S)(index, i)) * fprev[i][j] * ((int)level - 1);
            }
        }
    } // end corr_fe

    void corr_be(OdeCallbacks *ode, const size_t neq, const double dt, double *uold,
                 const double **fprev, const Matrix *S, const size_t index, const size_t level, double t,
                 double *unew)
    {
        // backwards euler update
        for (size_t j = 0; j < neq; j++)
        {
            uold[j] -= dt * (fprev[index + 1][j]);
            for (size_t i = 0; i < level; i++)
            {
                uold[j] += dt * ((*S)(index + 1, i) - (*S)(index, i)) * fprev[i][j] * ((int)level - 1);
            }
        }

        ode->step_cb(t, dt, uold, unew, neq);
    } // end corr_be
} // namespace ridc