#pragma once

#include <cassert>
#include <cstddef>

struct Matrix
{
    Matrix(size_t w, size_t h) : width(w), height(h), array(new double[w * h]{double{0.0}}) {}
    ~Matrix() { delete[] array; }
    double &operator()(int x, int y) const { return array[index(x, y)]; }
    double &at(int x, int y) const { return array[index(x,y)]; }
    double *row(int y) const { return &array[index(0, y)]; }
    size_t index(size_t x, size_t y) const
    {
        assert(x < width);
        assert(y < height);
        return x + width * y;
    }
    size_t width;
    size_t height;
    double *array;
};

extern "C"
{
    /**< RIDC helper function -- generates quadrature weight,
   int(L_{n,i}(x),x=a..b)
   @return quadrature weights
   @param a range of integration
   @param b range of integration
   @param Nx number of quadrature nodes
   @param L coefficients for Lagrange poly, L[0] + L[1]x + L[2]x^2 + ...
*/
    double get_quad_weight(double *L, int Nx, double a, double b);

    /**< RIDC helper function -- generates the coefficients for the
   lagrange interpolatory polynomials.
   @return (by reference) L: coefficients for the Lagrange
   intepolatory polynomial.  L is a vector of elements such that p(x)
   = L(0) + L(1)*x + L(2)*x^2 + ...
   @param x quadrature nodes
   @param i the i^{th} Lagrange polynomial
   @param Nx number of quadrature nodes
   @param L coefficients, returned by reference
*/

    void lagrange_coeff(double *x, int Nx, int i, double *L);

    /**< RIDC helper function -- constructions the integration matrix
   using get_quad_weight
   @return (by reference) the integration matrix S
   @param Nx number of quadrature nodes
   @param S integration matrix (by reference)
*/
    void integration_matrices(int N, Matrix *S);

    /**< RIDC helper function -- initializes uniformly spaced quadrature nodes
   @return (by reference) x: uniformly spaced quadrature nodes
   @param Nx number of quadrature nodes
   @param a range of integration
   @param b range of integration
   @param x quadrature node location (returned by reference)
*/
    void init_unif_nodes(double *x, int Nx, double a, double b);
}