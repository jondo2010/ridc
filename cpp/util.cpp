#include "util.h"

#include <stdio.h>
#include <vector>

#if 0
using namespace std;

void lagrange_coeff(
    const std::vector<double> &x,
    const int Nx,
    const int i,
    std::vector<double> &L)
{
  int k = 0;        // counter
  int j;            // counter
  int l;            // counter
  double denom = 1; // denominator for the coefficients

  // allocate memory, temporary storage variables
  std::vector<double> c(Nx);

  // set p(x) = 1
  c[0] = 1;
  for (j = 1; j < Nx; j++)
  {
    c[j] = 0.0;
  }

  for (j = 0; j < Nx; j++)
  {
    if (j != i)
    {
      // set p(x) = (x-x[j])*p(x); store in L
      L[0] = -x[j] * c[0];
      for (l = 1; l <= k; l++)
      {
        L[l] = c[l - 1] - x[j] * c[l];
      }
      k++;
      L[k] = 1;

      // update denominator
      denom = denom * (x[i] - x[j]);

      // copy from L back to c
      for (l = 0; l <= k; l++)
      {
        c[l] = L[l];
      }
    }
  }

  // scale coefficients correctly with denominator
  for (l = 0; l < Nx; l++)
  {
    L[l] = L[l] / denom;
  }
} // end lagrange_coeff

double get_quad_weight(const std::vector<double> &L, const int Nx, const double a, const double b)
{
  double answer = 0;
  double an = 1;
  double bn = 1;

  for (int j = 0; j < Nx; j++)
  {
    bn *= b;
    an *= a;
    answer += L[j] * (bn - an) / (j + 1);
  }

  return answer;
}

MatT integration_matrices(const int N)
{
  MatT S(N, std::vector<double>(N));

  // allocate memory, temporary storage variables
  std::vector<double> L(N);

  const auto x = init_unif_nodes(N, 0, 1);

  for (int i = 0; i < N; i++)
  {
    lagrange_coeff(x, N, i, L);
    for (int j = 0; j < N; j++)
    {
      S[j][i] = get_quad_weight(L, N, 0, x[j]);
      //printf("S[%i][%i][%i] = %f\n", N-1, j, i, S[j][i]);
    }
  }
  return S;
} // end integration_matrices

std::vector<double> init_unif_nodes(const int Nx, const double a, const double b)
{
  std::vector<double> x(Nx);

  const double h = (b - a) / (Nx - 1.0); // size of interval
  for (int j = 0; j < Nx; j++)
  {
    x[j] = a + j * h;
  }
  return x;
} // end init_unif_nodes
#endif

extern "C"
{
    /**
 * Generates quadrature weight, \int(L_{n,i}(x),x=a..b)
 * 
 * Inputs:
 *  - a,b: range of integration (0->y)
 *  - Nx: number of quadrature nodes
 *  - L[i]: coefficients for Lagrange poly, L[0] + L[1]x + L[2]x^2 + ...
 * 
 * Outputs: 
 *  - quadrature weight, w[i] = int(L_{n,i}(x),t=a..b)
 */
#if 0
  double get_quad_weight(double *L, int Nx, double a, double b)
  {
    int j; // counter

    double answer = 0;
    double an = 1;
    double bn = 1;

    for (j = 0; j < Nx; j++)
    {
      bn *= b;
      an *= a;
      answer += L[j] * (bn - an) / (j + 1);
    }

    return answer;
  } // end get_quad_weight
#endif

    /**
 * Generates coefficients for Lagrange interpolatory polynomials, L_{n,i}(x).
 * Used to generate weight w_i
 * 
 * Assumptions: nodes x are contained within [0,1], distinct
 * 
 * Inputs:
 * - x, (quadrature nodes)
 * - i, the i^{th} Lagrange polynomial
 * - Nx, number of quadrature nodes
 * 
 * Outputs: (By reference)
 *  - L, coefficients for the Lagrange intepolatory polynomial.
 *    L is a vector of elements such that p(x) = L(0) + L(1)*x + L(2)*x^2 + ...
 */
#if 0
  void lagrange_coeff(double *x, int Nx, int i, double *L)
  {

    int k = 0;        // counter
    int j;            // counter
    int l;            // counter
    double denom = 1; // denominator for the coefficients

    // allocate memory, temporary storage variables
    double *c = new double[Nx];

    // set p(x) = 1
    c[0] = 1;
    for (j = 1; j < Nx; j++)
    {
      c[j] = 0.0;
    }

    for (j = 0; j < Nx; j++)
    {
      if (j != i)
      {
        // set p(x) = (x-x[j])*p(x); store in L
        L[0] = -x[j] * c[0];
        for (l = 1; l <= k; l++)
        {
          L[l] = c[l - 1] - x[j] * c[l];
        }
        k++;
        L[k] = 1;

        // update denominator
        denom = denom * (x[i] - x[j]);

        // copy from L back to c
        for (l = 0; l <= k; l++)
        {
          c[l] = L[l];
        }
      }
    }

    // scale coefficients correctly with denominator
    for (l = 0; l < Nx; l++)
    {
      L[l] = L[l] / denom;
    }

    delete[] c;
  } // end lagrange_coeff
#endif

    /**
 * initialize uniformly spaced quadrature nodes
 * 
 * creates uniformly spaced nodes
 * 
 * Inputs:
 * - Nx, number of quadrature nodes
 * - a,b, domain where nodes are located
 * 
 * Outputs: (by reference)
 * - location of nodes, x
 */
#if 0
  void init_unif_nodes(double *x, int Nx, double a, double b)
  {
    double h; // size of interval
    int j;    //counter
    h = (b - a) / (Nx - 1.0);
    for (j = 0; j < Nx; j++)
    {
      x[j] = a + j * h;
    }
  } // end init_unif_nodes
#endif

    /**
 * Usage: generates integration matrices
 * 
 * Inputs:
 *  - N, number of quadrature nodes
 * Outputs: (by reference)
 *  - S = int(L_{n,i}(x),t=a..y[j]) stored as S[j,i]
 */
#if 0
  void integration_matrices(int N, Matrix *S)
  {
    int i; // counter
    int j; // counter

    double *x = new double[N];

    // allocate memory, temporary storage variables
    double *L = new double[N];

    init_unif_nodes(x, N, 0, 1);

    for (i = 0; i < N; i++)
    {
      lagrange_coeff(x, N, i, L);
      for (j = 0; j < N; j++)
      {
        (*S)(j, i) = get_quad_weight(L, N, 0, x[j]);
      }
    }

    delete[] x;
    delete[] L;
  } // end integration_matrices
#endif
}