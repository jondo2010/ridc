#include <cmath>
#include <stddef.h>

#include <gsl/gsl_linalg.h>

extern "C"
{
    void bruss_rhs(const double t, const double *u, double *f, const size_t neq);
    void bruss_step(const double t, const double dt, const double *u, double *unew, const size_t neq);
    static void bruss_newt(const double t, const double dt, const double *uprev, const double *uguess, double *g, const size_t neq);
    void bruss_jac(const double t, const double dt, const double *u, double *J, size_t neq);
}

void bruss_rhs(const double t, const double *u, double *f, const size_t neq)
{
    /** user implemented rhs function, u'=rhs(t,u) 
       @return (by reference) f: rhs(t,u)
       @param t current time step
       @param u solution u at time t
       @param f rhs(t,u)
    */
    double A = 1.0;
    double B = 3.0;
    double alpha = 0.02;
    double dx = 1.0 / (neq + 1);
    int Nx = neq / 2;

    f[0] = A + u[0] * u[0] * u[Nx] - (B + 1.0) * u[0] + alpha / dx / dx * (u[1] - 2 * u[0] + 1.0);
    f[Nx - 1] = A + u[Nx - 1] * u[Nx - 1] * u[2 * Nx - 1] - (B + 1.0) * u[Nx - 1] + alpha / dx / dx * (1.0 - 2 * u[Nx - 1] + u[Nx - 2]);

    f[Nx] = B * u[0] - u[0] * u[0] * u[Nx] + alpha / dx / dx * (u[Nx + 1] - 2 * u[Nx] + 3.0);
    f[2 * Nx - 1] = B * u[Nx - 1] - u[Nx - 1] * u[Nx - 1] * u[2 * Nx - 1] + alpha / dx / dx * (3.0 - 2 * u[2 * Nx - 1] + u[2 * Nx - 2]);

    for (int i = 1; i < Nx - 1; i++)
    {
        f[i] = A + u[i] * u[i] * u[Nx + i] - (B + 1.0) * u[i] + alpha / dx / dx * (u[i + 1] - 2 * u[i] + u[(int)i - 1]);
        f[Nx + i] = B * u[i] - u[i] * u[i] * u[Nx + i] + alpha / dx / dx * (u[Nx + i + 1] - 2 * u[Nx + i] + u[Nx + (int)i - 1]);
    }
}

void bruss_step(const double t, const double dt, const double *u, double *unew, const size_t neq)
{
    /** user implemented step function, for advancing the solution from t to t+dt 
       @return (by reference) unew: solution at time t+dt
       @param t current time step
       @param u solution u at time t
       @param unew solution at time t+dt
    */
    double tnew = t + dt;

    double NEWTON_TOL = 1.0e-14;
    int NEWTON_MAXSTEP = 1000;

    double *J;
    double *stepsize;
    double *uguess;

    stepsize = new double[neq];
    uguess = new double[neq];

    J = new double[neq * neq];

    // GSL
    gsl_matrix_view m;
    gsl_vector_view b;
    gsl_vector *x = gsl_vector_alloc(neq);
    gsl_permutation *p = gsl_permutation_alloc(neq);

    // set initial condition
    for (size_t i = 0; i < neq; i++)
    {
        uguess[i] = u[i];
    }

    double maxstepsize;

    int counter = 0;
    int *pivot = new int[neq];

    while (true)
    {
        // create rhs
        bruss_newt(tnew, dt, u, uguess, stepsize, neq);

        // compute numerical Jacobian
        bruss_jac(tnew, dt, uguess, J, neq);

        m = gsl_matrix_view_array(J, neq, neq);
        b = gsl_vector_view_array(stepsize, neq);

        int s; // presumably, this is some flag info

        gsl_linalg_LU_decomp(&m.matrix, p, &s);
        gsl_linalg_LU_solve(&m.matrix, p, &b.vector, x);

        // check for convergence
        maxstepsize = 0.0;
        for (size_t i = 0; i < neq; i++)
        {
            uguess[i] = uguess[i] - x->data[i];
            if (std::abs(stepsize[i]) > maxstepsize)
            {
                maxstepsize = std::abs(x->data[i]);
            }
        }

        // if update sufficiently small enough
        if (maxstepsize < NEWTON_TOL)
        {
            break;
        }

        counter++;
        //error if too many steps
        if (counter > NEWTON_MAXSTEP)
        {
            fprintf(stderr, "max Newton iterations reached\n");
            exit(42);
        }
    } // end Newton iteration

    for (size_t i = 0; i < neq; i++)
    {
        unew[i] = uguess[i];
    }

    delete[] pivot;
    delete[] uguess;
    delete[] stepsize;
    delete[] J;

    gsl_permutation_free(p);
    gsl_vector_free(x);
}

static void bruss_newt(const double t, const double dt, const double *uprev, const double *uguess, double *g, const size_t neq)
{
    /** Helper function to compute the next Newton step
       for solving a system of equations
       
       @return (by reference) g how far from zero we are
       @param t current time step
       @param uguess current solution guess
       @param uprev solution at previous time step
       @param g how far from zero we are, returned by reference
    */
    bruss_rhs(t, uguess, g, neq);
    for (size_t i = 0; i < neq; i++)
    {
        g[i] = uguess[i] - uprev[i] - dt * g[i];
    }
}

void bruss_jac(const double t, const double dt, const double *u, double *J, size_t neq)
{
    /** Helper function to the Jacobian matrix (using finite differences)
       for advancing the solution from time t(n) to t(n+1) using an
       implicit Euler step on a system of equations
       
       @return (by reference) J the Jacobian for the Newton step
       @param t current time step
       @param u function value at the current time step
       @param J Jacobian, returned by reference
    */
    double d = 1e-5; // numerical Jacobian approximation
    double *u1;
    double *f1;
    double *f;

    // need to allocate memory
    u1 = new double[neq];
    f1 = new double[neq];
    f = new double[neq];

    // note, instead of using newt, use rhs for efficiency
    bruss_rhs(t, u, f, neq);

    for (size_t i = 0; i < neq; i++)
    {
        for (size_t j = 0; j < neq; j++)
        {
            u1[j] = u[j];
        }
        u1[i] = u1[i] + d;

        bruss_rhs(t, u1, f1, neq);

        for (size_t j = 0; j < neq; j++)
        {
            J[j * neq + i] = -dt * (f1[j] - f[j]) / d;
        }
        J[i * neq + i] = 1.0 + J[i * neq + i];
    }

    // need to delete memory
    delete[] u1;
    delete[] f1;
    delete[] f;
}