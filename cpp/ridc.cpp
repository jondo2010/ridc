#include "ridc.h"

#include <algorithm>
#include <cmath>
#include <stdio.h>
#include <vector>

extern "C"
{
    struct OrderState
    {
        const size_t index = 0;

        // setup memory stencil for RIDC
        size_t stencilSize;
        // memory footprint for quadrature approximations, for all levels
        double **u;
        double **f;
        double **fprev;

        // this is a temporary variable for storing the solution at the new time step (for each level)
        double *unew;
        double *fnew;

        // setting variables for integration matrices
        Matrix *S;

        // counter for where the node is for each correction level
        size_t ii = 0;

        // variable containing flag for whether to shift memory in stencil
        bool doMemShift = false;

        // flag for whether the node is able to compute the next step for
        // each correction level
        bool doComputeStep;

        bool *filter;

        OrderState(
            const size_t index, /// The index of this OrderState
            const size_t order, /// Total order of the RIDC
            const size_t startnum,
            const size_t neq /// Number of equations
            )
            : index(index), stencilSize((index < (order - 1)) ? index + 2 : 1)
        {
            u = new double *[stencilSize];
            f = new double *[stencilSize];
            fprev = new double *[stencilSize];
            for (size_t i = 0; i < stencilSize; i++)
            {
                u[i] = new double[neq];
                f[i] = new double[neq];
            }

            unew = new double[neq];
            fnew = new double[neq];

            S = new Matrix(index + 1, index + 1);
            filter = new bool[startnum];
        }
    };

    void init_filter(const size_t order, const size_t startnum, std::vector<OrderState> &states)
    {
        // set all of them to zero
        for (size_t p = 0; p < order; p++)
        {
            for (size_t q = 0; q < startnum; q++)
            {
                states[p].filter[q] = false;
            }
        }

        if (order == 1)
        {
            states[0].filter[1] = true;
        }
        else
        {
            size_t q = 0;
            for (size_t p = 1; p < order; p++)
            {
                // all except level p step
                for (size_t pp = 0; pp < p; pp++)
                {
                    states[pp].filter[q] = true;
                }

                q++;

                // one step
                for (size_t count = 0; count < p - 1; count++)
                {
                    states[p].filter[q] = true;
                    q++;
                }

                // all step, including level p
                for (size_t pp = 0; pp <= p; pp++)
                {
                    states[pp].filter[q] = true;
                }
                q++;
            } // loop over order
        }     // create filter
    }

    void inner_loop(
        OdeCallbacks *ode,
        const CorrectionCb corrector, /// Correction step callback
        const double ti,
        const double dt,
        const size_t neq,   /// number of equations
        const size_t Nt,    /// number of time steps
        const size_t order, /// RIDC order
        const size_t startnum,
        const size_t i,
        OrderState &state)
    {
        state.doComputeStep = false;
        state.doMemShift = false;

        if ((state.ii < Nt) && ((i >= ((int)startnum - 1)) || state.filter[i]))
        {
            state.doComputeStep = true;
        }

        if (state.doComputeStep)
        {
            // Now do a step of the pth Corrector/Predictor
            size_t index_into_array = std::min((int)state.ii, (int)state.stencilSize - 1);
            // compute current time
            double t = ti + (state.ii) * dt;
            if (state.index == 0)
            {
                // prediction step
                ode->step_cb(t, dt, state.u[index_into_array], state.unew, neq);

                // populate memory stencil for f (needed for order > 1)
                ode->rhs_cb(t + dt, state.unew, state.fnew, neq);
            }
            else
            {
                size_t index_sth = std::min((int)state.ii, (int)state.index - 1);

                // correction step!
                corrector(
                    ode,
                    neq,
                    dt,
                    state.u[index_into_array],
                    const_cast<const double **>(state.fprev),
                    //const_cast<const double **>(states[(int)p-1].f),
                    state.S,
                    index_sth,  // index
                    state.index + 1, // level
                    t,
                    state.unew);

                // populate memory stencil if not the last correction loop
                if ((int)state.index < ((int)order - 1))
                {
                    ode->rhs_cb(t + dt, state.unew, state.fnew, neq);
                }
            }
            state.ii++;

            if (state.ii >= state.stencilSize)
            {
                state.doMemShift = true;
            }
        } // end if doComputeStep[p]
    }

    void ridc(OdeCallbacks *ode,
              CorrectionCb corrector, /// Correction step callback
              const double dt,        /// time step
              const double ti,        /// initial time
              const double tf,        /// final time
              const size_t neq,       /// number of equations
              const size_t Nt,        /// number of time steps
              const size_t order,     /// RIDC order
              double *sol             /// Solution and initial condition
    )
    {

        // after startnum, all the nodes should be computing
        const size_t startnum = (order == 1) ? 1 : (order * (order + 1) / 2 - 1);

        // memory footprint for quadrature approximations, for all levels
        std::vector<OrderState> states;
        for (size_t p = 0; p < order; p++)
        {
            states.emplace_back(p, order, startnum, neq);
        }

        init_filter(order, startnum, states);

        // initialize integration matrices
        for (size_t j = 1; j < order; j++)
        {
            integration_matrices(j + 1, states[j].S);
        }

        // get initial condition
        for (size_t i = 0; i < neq; i++)
        {
            states[0].u[0][i] = sol[i];
        }

        // compute f[0][0]
        ode->rhs_cb(ti, states[0].u[0], states[0].f[0], neq);

        // copy initial condition for each RIDC level
        for (size_t j = 1; j < order; j++)
        {
            for (size_t i = 0; i < neq; i++)
            {
                states[j].u[0][i] = states[0].u[0][i];
                states[j].f[0][i] = states[0].f[0][i];
            }
            states[j].fprev = states[(int)j - 1].f;
        }

        ///////////////////////////////////////////////////////////////////
        // START-UP ROUTINES
        ///////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////
        // MAIN TIME LOOP
        ///////////////////////////////////////////////////////////////////

        int i = 0; // counter
        while (1)
        {
            ///////////////////////////////////////////////////////////////////
            // PIPELINE UPDATE
            ///////////////////////////////////////////////////////////////////

            for (OrderState &state : states)
            {
                inner_loop(ode, corrector, ti, dt, neq, Nt, order, startnum, i, state);
            } // end pragma for loop

            // memshifts
            for (size_t p = 0; p < order; p++)
            {
                if (states[p].doComputeStep)
                {
                    if (!(states[p].doMemShift))
                    {
                        size_t index_into_array = states[p].ii;
                        double *temp;

                        // Effect: place unew into correct spot in u
                        temp = states[p].u[index_into_array];
                        states[p].u[index_into_array] = states[p].unew;
                        states[p].unew = temp;

                        // Effect: place fnew into correct spot in f
                        temp = states[p].f[index_into_array];
                        states[p].f[index_into_array] = states[p].fnew;
                        states[p].fnew = temp;
                    }
                    else
                    {
                        // Shuffle all state vectors down, then push unew to the top.
                        double *temp;
                        temp = states[p].u[0];
                        for (int k = 0; k < ((int)states[p].stencilSize - 1); k++)
                        {
                            states[p].u[k] = states[p].u[k + 1];
                        }
                        states[p].u[(int)states[p].stencilSize - 1] = states[p].unew;
                        states[p].unew = temp;

                        temp = states[p].f[0];
                        for (int k = 0; k < ((int)states[p].stencilSize - 1); k++)
                        {
                            states[p].f[k] = states[p].f[k + 1];
                        }
                        states[p].f[(int)states[p].stencilSize - 1] = states[p].fnew;
                        states[p].fnew = temp;
                        //printf("swap f[%i]<->fnew[%i]\n", p, p);
                    } // end doMemShift

                } // end doComputeStep
            }     // end p

            if (states[(int)order - 1].ii == Nt)
            {
                break;
            }
            i++;

        } // end while loop

        ///////////////////////////////////////////////////////////////////
        // END: MAIN TIME LOOP
        ///////////////////////////////////////////////////////////////////

        size_t p = order - 1;
        for (size_t i = 0; i < neq; i++)
        {
            sol[i] = states[p].u[states[p].stencilSize - 1][i];
        }

        // clear memory
        for (size_t p = 0; p < order; p++)
        {
            delete[] states[p].unew;
            delete[] states[p].fnew;

            delete[] states[p].filter;
        }

        for (size_t p = 0; p < order; p++)
        {
            for (size_t j = 0; j < states[p].stencilSize; j++)
            {
                delete[] states[p].u[j];
                delete[] states[p].f[j];
            }
            delete[] states[p].u;
            delete[] states[p].f;
        }

        for (size_t p = 0; p < order; p++)
        {
            delete states[p].S;
        }
    }
} // end ridc_fe