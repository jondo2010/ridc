#pragma once

#include "util.h"

extern "C"
{
    struct OdeCallbacks
    {
        /**
         * User implemented rhs function, u'=rhs(t,u)
         * @return (by reference) f: rhs(t,u)
         * @param t current time step
         * @param u solution u at time t
         * @param f rhs(t,u)
         */
        void (*rhs_cb)(const double t, const double *u, double *f, const size_t neq);

        /**
         * User implemented step function, for advancing the solution from t to t+dt
         * @return (by reference) unew: solution at time t+dt
         * @param t current time step
         * @param u solution u at time t
         * @param unew solution at time t+dt
         */
        void (*step_cb)(const double t, const double dt, double *u, double *unew, const size_t neq);
    };

    typedef void (*CorrectionCb)(OdeCallbacks *ode,
                                 const size_t neq,     /// number of equations
                                 const double dt,      /// time step
                                 double *uold,         /// solution at time level t
                                 const double **fprev, /// matrix containing derivative information from previous steps, previous level
                                 const Matrix *S,      /// integration matrix (quadrature weights)
                                 const size_t index,   /// decides which quadrature weights to use
                                 const size_t level,   /// determines size of quadrature stencil
                                 double t,             /// current time iterate
                                 double *unew          /// solution at the new time level, passed by reference
    );

    /**
 * Main ridc loop.
 * Initializes variables, integrates solution from ti to tf by bootstrapping the step function.
 * 
 * @return (by reference) sol, the solution at the final time, param.tf
 * @param ode abstract class containing parameters and step/rhs functions
 * @param order order of the RIDC method (predictor + number of correctors)
 * @param sol initial condition of the IVP
 */
    void ridc(OdeCallbacks *ode,
              CorrectionCb corrector, /// Correction step callback
              const double dt,        /// time step
              const double ti,        /// initial time
              const double tf,        /// final time
              const size_t neq,       /// number of equations
              const size_t Nt,        /// number of time steps
              const size_t order,     /// RIDC order
              double *sol             /// Solution and initial condition
    );

    /**
     * RIDC helper function - solves error equation, updating the solution from time t to time t+param.dt.
     * @return (by reference) unew: solution at time level t + param.dt
     * @param ode abstract class containing parameters and step/rhs functions
     */
    void corr_fe(OdeCallbacks *ode,
                 const size_t neq,     /// number of equations
                 const double dt,      /// time step
                 double *uold,         /// solution at time level t
                 const double **fprev, /// matrix containing derivative information from previous steps, previous level
                 const Matrix *S,      /// integration matrix (quadrature weights)
                 const size_t index,   /// decides which quadrature weights to use
                 const size_t level,   /// determines size of quadrature stencil
                 double t,             /// current time iterate
                 double *unew          /// solution at the new time level, passed by reference
    );

    /**
     * RIDC helper function - solves error equation, updating the solution from time t to time t+param.dt.
     * @return (by reference) unew: solution at time level t + param.dt
     * @param ode abstract class containing parameters and step/rhs functions
     */
    void corr_be(OdeCallbacks *ode,
                 const size_t neq,     /// number of equations
                 const double dt,      /// time step
                 double *uold,         /// solution at time level t
                 const double **fprev, /// matrix containing derivative information from previous steps, previous level
                 const Matrix *S,      /// integration matrix (quadrature weights)
                 const size_t index,   /// decides which quadrature weights to use
                 const size_t level,   /// determines size of quadrature stencil
                 double t,             /// current time iterate
                 double *unew          /// solution at the new time level, passed by reference
    );
}