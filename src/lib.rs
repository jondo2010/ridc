#![warn(clippy::all)]
#![allow(dead_code, non_camel_case_types, non_snake_case)]
#![feature(min_const_generics)]

pub mod cpp;
mod ridc;
mod util;
mod window_pair;

use std::fmt::Debug;

use tracing::instrument;

pub use crate::ridc::Solver;

pub trait Steppable {
    const D: f64 = 1e-5; // numerical Jacobian approximation

    /// RHS function, u'=rhs(t,u)
    /// # Arguments
    /// * `t` - Current time step
    /// * `u` - Solution u at time t
    /// * `f` - rhs(t,u) return (by reference)
    fn rhs(t: f64, u: ndarray::ArrayView1<f64>, f: ndarray::ArrayViewMut1<f64>);

    /// Step function, for advancing the solution from t to t+dt
    /// # Arguments
    /// * `t current time step
    /// * `u solution u at time t
    /// * `unew solution at time t+dt return (by reference)
    fn step(t: f64, dt: f64, u: ndarray::ArrayView1<f64>, u_new: ndarray::ArrayViewMut1<f64>);

    /// Compute the Jacobian at time t
    fn jac(
        t: f64,
        dt: f64,
        u: ndarray::ArrayView1<f64>,
        mut mat_j: ndarray::ArrayViewMut2<f64>, // Storage for Jacobian
    ) {
        // f holds the derivative rhs(t,u)
        let mut f = ndarray::Array1::zeros(u.raw_dim());
        // f1 holds the derivative rhs(t,u1)
        let mut f1 = ndarray::Array1::zeros(u.raw_dim());
        // u1 holds the perterbed u vector
        let mut u1 = ndarray::Array1::zeros(u.raw_dim());

        Self::rhs(t, u, f.view_mut());
        for i in 0..u.len() {
            // Reset u1 to u, then perterb the current index
            u1.assign(&u);
            u1[i] += Self::D;

            Self::rhs(t, u1.view(), f1.view_mut());
            f1 -= &f;
            f1 *= dt / Self::D;
            mat_j.column_mut(i).assign(&f1);
        }
    }
}

pub trait Corrector {
    /// RIDC helper function - solves error equation, updating the solution from time t to time t+param.dt.
    /// # Arguments
    /// * `u_old` - Solution at time level t
    /// * `f_prev`
    /// * `mat_s`
    fn correction_step<Ode: Steppable + Sync + Debug>(
        u_old: ndarray::ArrayView1<f64>,
        f_prev: ndarray::ArrayView2<f64>,
        mat_s: ndarray::ArrayView2<f64>,
        index: usize,
        level: usize,
        t: f64,
        dt: f64,
        u_new: ndarray::ArrayViewMut1<f64>,
    );
}

pub struct ForwardEuler;
impl Corrector for ForwardEuler {
    #[instrument]
    fn correction_step<Ode: Steppable + Sync + Debug>(
        u_old: ndarray::ArrayView1<f64>,
        f_prev: ndarray::ArrayView2<f64>,
        mat_s: ndarray::ArrayView2<f64>,
        index: usize,
        level: usize,
        t: f64,
        dt: f64,
        mut u_new: ndarray::ArrayViewMut1<f64>,
    ) {
        Ode::step(t, dt, u_old, u_new.view_mut());

        // forward euler update
        for j in 0..u_new.len() {
            u_new[j] = u_new[j] - (dt * f_prev[[index, j]]);
            for i in 0..level {
                u_new[j] = u_new[j]
                    + dt * (mat_s[[index + 1, i]] - mat_s[[index, i]])
                        * f_prev[[i, j]]
                        * (level as f64 - 1.0);
            }
        }
    }
}

pub struct BackwardEuler;
impl Corrector for BackwardEuler {
    #[instrument]
    fn correction_step<ODE: Steppable + Sync>(
        u_old: ndarray::ArrayView1<f64>,
        f_prev: ndarray::ArrayView2<f64>,
        mat_s: ndarray::ArrayView2<f64>,
        index: usize,
        level: usize,
        t: f64,
        dt: f64,
        u_new: ndarray::ArrayViewMut1<f64>,
    ) {
        // Make a local copy of u_old for the backwards step
        let mut u_old = u_old.to_owned();
        // backwards euler update

        ndarray::Zip::indexed(&mut u_old).apply(|j, u_old|{
            *u_old -= dt * f_prev[[index + 1, j]];
            for i in 0..level {
                *u_old += dt
                    * (mat_s[[index + 1, i]] - mat_s[[index, i]])
                    * f_prev[[i, j]]
                    * (level as f64 - 1.0);
            }
        });

        ODE::step(t, dt, u_old.view(), u_new);
    }
}
