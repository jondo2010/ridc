pub struct WindowPairMut<'a, T: 'a> {
    v: &'a mut [T],
    ix: usize,
}
impl<'a, T> WindowPairMut<'a, T> {
    pub fn next(&mut self) -> Option<(&mut T, &mut T)> {
        if self.ix < (self.v.len() - 1) {
            let (t0, t1) = (&mut self.v[self.ix..=self.ix + 1])
                .split_first_mut()
                .unwrap();
            let ret = Some((t0, t1.first_mut().unwrap()));
            self.ix += 1;
            ret
        } else {
            None
        }
    }
}

pub trait AsWindowPairMut<'a, T> {
    fn windows_pair_mut(&'a mut self) -> WindowPairMut<'a, T>;
}

impl<'a, T, Q: AsMut<[T]>> AsWindowPairMut<'a, T> for Q {
    fn windows_pair_mut(&'a mut self) -> WindowPairMut<'a, T> {
        WindowPairMut {
            v: self.as_mut(),
            ix: 0,
        }
    }
}

#[test]
fn test_windows_pair_mut() {
    let mut v = vec![1, 2, 3, 4, 5];
    let mut windows = v.windows_pair_mut();
    while let Some((win0, win1)) = windows.next() {
        *win0 += *win1;
    }
    assert_eq!(&v, &[3, 5, 7, 9, 5]);
}
