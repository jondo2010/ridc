#![allow(dead_code, non_camel_case_types, non_snake_case)]

use ndarray::prelude::*;
use ndarray::ShapeBuilder;

extern "C" {
    /// Main ridc loop.
    /// Initializes variables, integrates solution from ti to tf by bootstrapping the step function.
    ///
    /// # Arguments
    /// * `ode`     - abstract class containing parameters and step/rhs functions
    /// * `order`   - order of the RIDC method (predictor + number of correctors)
    /// * `sol`     - initial condition of the IVP, state variable for output.
    pub fn ridc(
        ode: *mut OdeCallbacks,
        corrector: CorrectionCb,
        dt: f64,
        ti: f64,
        tf: f64,
        neq: libc::size_t,
        Nt: libc::size_t,
        order: libc::size_t,
        sol: *mut f64,
    );

    pub fn corr_fe(
        ode: *mut OdeCallbacks,
        neq: libc::size_t,
        dt: f64,
        uold: *mut f64,
        fprev: *mut *mut f64,
        S: *const Matrix,
        index: ::std::os::raw::c_int,
        level: ::std::os::raw::c_int,
        t: f64,
        unew: *mut f64,
    );

    pub fn corr_be(
        ode: *mut OdeCallbacks,
        neq: libc::size_t,
        dt: f64,
        uold: *mut f64,
        fprev: *mut *mut f64,
        S: *const Matrix,
        index: ::std::os::raw::c_int,
        level: ::std::os::raw::c_int,
        t: f64,
        unew: *mut f64,
    );
}

#[repr(C)]
pub struct Matrix {
    width: libc::size_t,
    height: libc::size_t,
    data: *mut libc::c_double,
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct OdeCallbacks {
    /// User implemented rhs function, u'=rhs(t,u)
    /// # Arguments
    /// * `t`   - current time step
    /// * `dt`      - size of step
    /// * `u`   - solution u at time t
    /// * `f`   - rhs(t,u)
    /// * `neq` - number of equations
    pub rhs_cb: ::std::option::Option<
        unsafe extern "C" fn(t: f64, u: *const f64, f: *mut f64, neq: libc::size_t),
    >,
    /// User implemented step function, for advancing the solution from t to t+dt
    /// # Arguments
    /// * `t`       - current time step
    /// * `dt`      - size of step
    /// * `u`       - solution u at time t
    /// * `unew`    - solution at time t+dt
    /// * `neq`     - number of equations
    pub step_cb: ::std::option::Option<
        unsafe extern "C" fn(t: f64, dt: f64, u: *mut f64, unew: *mut f64, neq: libc::size_t),
    >,
}

pub type CorrectionCb = ::std::option::Option<
    unsafe extern "C" fn(
        ode: *mut OdeCallbacks,
        neq: libc::size_t,
        dt: f64,
        uold: *mut f64,
        fprev: *mut *mut f64,
        S: *const Matrix,
        index: ::std::os::raw::c_int,
        level: ::std::os::raw::c_int,
        t: f64,
        unew: *mut f64,
    ),
>;

/*
#[no_mangle]
pub extern "C" fn get_quad_weight(
    L: *const libc::c_double,
    Nx: libc::c_int,
    a: libc::c_double,
    b: libc::c_double,
) -> libc::c_double {
    let lagrange_coeffs = unsafe { std::slice::from_raw_parts(L, Nx as usize) };
    super::util::get_quad_weight(lagrange_coeffs, a..b)
}

/// Generates the coefficients for the lagrange interpolatory polynomials.
/// L is a vector of elements such that p(x) = L(0) + L(1)*x + L(2)*x^2 + ...
/// # Arguments
/// * `x`   - quadrature nodes
/// * `i`   - the i^{th} Lagrange polynomial
/// * `Nx`  - number of quadrature nodes
/// * `L`   - coefficients, returned by reference
#[no_mangle]
pub extern "C" fn lagrange_coeff(
    x: *const libc::c_double,
    Nx: libc::c_int,
    i: libc::c_int,
    L: *mut libc::c_double,
) {
    let quad_nodes = unsafe { std::slice::from_raw_parts(x, Nx as usize) };
    let lagrange_coeffs = unsafe { std::slice::from_raw_parts_mut(L, Nx as usize) };
    super::util::lagrange_coeff(quad_nodes, i as usize, lagrange_coeffs)
}

#[no_mangle]
pub extern "C" fn init_unif_nodes(
    x: *mut libc::c_double,
    Nx: libc::c_int,
    a: libc::c_double,
    b: libc::c_double,
) {
    let quad_nodes = unsafe { std::slice::from_raw_parts_mut(x, Nx as usize) };
    super::util::init_unif_nodes(quad_nodes, a..b)
}
*/

#[no_mangle]
pub extern "C" fn integration_matrices(N: libc::c_int, S: *mut Matrix) {
    let mut mat_s = unsafe {
        let Matrix {
            width,
            height,
            data,
        } = *S;
        ArrayViewMut::from_shape_ptr((width, height).f(), data)
    };
    super::util::integration_matrices(N as usize, mat_s.view_mut());
}

pub unsafe extern "C" fn corrector_forwards(
    ode: *mut OdeCallbacks,
    neq: libc::size_t,
    dt: f64,
    uold: *mut f64,
    fprev: *mut *mut f64,
    S: *const Matrix,
    index: ::std::os::raw::c_int,
    level: ::std::os::raw::c_int,
    t: f64,
    unew: *mut f64,
) {
    let index = index as usize;
    let level = level as usize;
    let mat_s = {
        let Matrix {
            width,
            height,
            data,
        } = *S;
        ArrayView::from_shape_ptr((width, height).f(), data)
    };
    let _u_old = ArrayView1::from_shape_ptr(neq, uold);
    let mut u_new = ndarray::ArrayViewMut1::from_shape_ptr(neq, unew);
    let mut f_prev = ndarray::Array2::zeros((level, neq));
    for (i, &ff) in std::slice::from_raw_parts(fprev, level).iter().enumerate() {
        f_prev
            .row_mut(i)
            .assign(&ndarray::ArrayView1::from_shape_ptr(neq, ff))
    }

    // forward euler update
    (*ode).step_cb.unwrap()(t, dt, uold, unew, neq);
    for j in 0..neq {
        u_new[j] -= dt * f_prev[[index, j]];
        for i in 0..level {
            u_new[j] += dt
                * (mat_s[[index + 1, i]] - mat_s[[index, i]])
                * f_prev[[i, j]]
                * (level as f64 - 1.0);
        }
    }
}

pub unsafe extern "C" fn corrector_backwards(
    ode: *mut OdeCallbacks,
    neq: libc::size_t,
    dt: f64,
    uold: *mut f64,
    fprev: *mut *mut f64,
    S: *const Matrix,
    index: ::std::os::raw::c_int,
    level: ::std::os::raw::c_int,
    t: f64,
    unew: *mut f64,
) {
    let index = index as usize;
    let level = level as usize;
    let mat_s = {
        let Matrix {
            width,
            height,
            data,
        } = *S;
        ArrayView::from_shape_ptr((width, height).f(), data)
    };
    let u_old = ArrayView1::from_shape_ptr(neq, uold);
    let mut _u_new = ndarray::ArrayViewMut1::from_shape_ptr(neq, unew);
    let mut f_prev = ndarray::Array2::zeros((level, neq));
    for (i, &ff) in std::slice::from_raw_parts(fprev, level).iter().enumerate() {
        f_prev
            .row_mut(i)
            .assign(&ndarray::ArrayView1::from_shape_ptr(neq, ff))
    }

    // backwards euler update
    // Make a local copy of u_old for the backwards step
    let mut u_old = u_old.to_owned();
    for j in 0..neq {
        u_old[j] -= dt * f_prev[[index + 1, j]];
        for i in 0..level {
            u_old[j] += dt
                * (mat_s[[index + 1, i]] - mat_s[[index, i]])
                * f_prev[[i, j]]
                * (level as f64 - 1.0);
        }
    }

    (*ode).step_cb.unwrap()(t, dt, u_old.as_mut_ptr(), unew, neq);
}
