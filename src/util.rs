use ndarray::prelude::*;

pub(crate) mod old {
    use super::*;
    /// Generates coefficients for Lagrange interpolatory polynomials, L_{n,i}(x).
    /// Used to generate weight w_i
    ///
    /// Assumptions: nodes x are contained within [0,1], distinct
    ///
    /// Inputs:
    ///     x, (quadrature nodes)
    ///     i, the i^{th} Lagrange polynomial
    ///     Nx, number of quadrature nodes
    ///
    /// Returns:
    ///     L, coefficients for the Lagrange intepolatory polynomial.
    ///     L is a vector of elements such that p(x) = L(0) + L(1)*x + L(2)*x^2 + ...
    fn lagrange_coeff(x: ArrayView1<f64>, i: usize) -> Array1<f64> {
        let mut k = 0; // counter
        let mut denom = 1.0; // denominator for the coefficients

        // allocate memory, temporary storage variables
        //double * c = new double[Nx];
        let mut lag: Array1<f64> = ArrayBase::zeros(x.raw_dim());
        let mut c: Array<f64, _> = ArrayBase::zeros(x.raw_dim());

        // set p(x) = 1
        c[0] = 1.0;

        for j in 0..x.len() {
            if j != i {
                // set p(x) = (x-x[j])*p(x); store in L
                lag[0] = -x[j] * c[0];
                for l in 1..=k {
                    lag[l] = c[l - 1] - x[j] * c[l];
                }
                k += 1;
                lag[k] = 1.0;

                // update denominator
                denom *= x[i] - x[j];

                // copy from L back to c
                for l in 0..=k {
                    c[l] = lag[l];
                }
            }
        }

        // scale coefficients correctly with denominator
        lag *= denom.recip();

        lag
    }

    /// generates quadrature weight, \int(L_{n,i}(x),x=a..b)
    /// Inputs:
    ///     a..b: range of integration (0->y)
    ///     L[i]: coefficients for Lagrange poly, L[0] + L[1]x + L[2]x^2 + ...
    ///
    /// Outputs:
    ///     quadrature weight, w[i] = int(L_{n,i}(x),t=a..b)
    fn get_quad_weight(L: ArrayView1<f64>, ab: std::ops::Range<f64>) -> f64 {
        let mut answer = 0.0;
        let mut an = 1.0;
        let mut bn = 1.0;

        for (j, ll) in L.indexed_iter() {
            bn *= ab.end;
            an *= ab.start;
            answer += ll * (bn - an) / ((j + 1) as f64);
        }

        return answer;
    }

    /// Generate integration matrices
    /// Rturns: S = int(L_{n,i}(x),t=a..y[j]) stored as S[j,i]
    pub fn integration_matrices(N: usize) -> Array2<f64> {
        let x = Array::linspace(0.0, 1.0, N);
        let mut mat_s = Array::zeros((N, N));

        for i in 0..N {
            let lag = lagrange_coeff(x.view(), i);
            for j in 0..N {
                mat_s[[j, i]] = get_quad_weight(lag.view(), 0.0..x[j]);
            }
        }

        mat_s
    }
}

pub fn lagrange_coeff<T>(quad_nodes: &[T], poly_i: usize, lagrange_coeffs: &mut [T])
where
    T: num_traits::float::Float + num_traits::FromPrimitive,
{
    let mut k = 0; // counter
    let mut denom = T::one(); // denominator for the coefficients

    // allocate memory, temporary storage variables
    let mut c = Vec::with_capacity(quad_nodes.len());
    c.resize(quad_nodes.len(), T::zero());

    // set p(x) = 1
    c[0] = T::one();

    for j in 0..quad_nodes.len() {
        if j != poly_i {
            // set p(x) = (x-x[j])*p(x); store in L
            lagrange_coeffs[0] = -quad_nodes[j] * c[0];
            for l in 1..=k {
                lagrange_coeffs[l] = c[l - 1] - quad_nodes[j] * c[l];
            }
            k += 1;
            lagrange_coeffs[k] = T::one();

            // update denominator
            denom = denom * (quad_nodes[poly_i] - quad_nodes[j]);

            // copy from L back to c
            for l in 0..=k {
                c[l] = lagrange_coeffs[l];
            }
        }
    }

    // scale coefficients correctly with denominator
    for coeff in lagrange_coeffs.iter_mut() {
        *coeff = *coeff * denom.recip();
    }
}

pub fn get_quad_weight<T>(lagrange_coeffs: &[T], ab: std::ops::Range<T>) -> T
where
    T: num_traits::float::Float + num_traits::FromPrimitive,
{
    let (answer, _, _, _) =
        lagrange_coeffs
            .iter()
            .fold((T::zero(), T::one(), T::one(), T::zero()), |acc, ll| {
                let (mut answer, mut an, mut bn, mut jj) = acc;
                an = an * ab.start;
                bn = bn * ab.end;
                jj = jj + T::one();
                answer = answer + (*ll * (bn - an) / jj);
                (answer, an, bn, jj)
            });
    answer
}

/// Usage: generates integration matrices
///
/// # Arguments
/// * Nx - number of quadrature nodes
/// * mat_s - = int(L_{n,i}(x),t=a..y[j]) stored as S[j,i]
pub fn integration_matrices<T>(Nx: usize, mut mat_s: ArrayViewMut2<T>)
where
    T: num_traits::float::Float + num_traits::FromPrimitive + std::fmt::Debug,
{
    let quad_nodes = Array::linspace(T::zero(), T::one(), Nx);
    let quad_nodes = quad_nodes.as_slice().unwrap();
    let mut lagrange_coeffs = Array::zeros(Nx);
    let lagrange_coeffs = lagrange_coeffs.as_slice_mut().unwrap();

    for poly_i in 0..Nx {
        lagrange_coeff(quad_nodes, poly_i, lagrange_coeffs);
        for poly_j in 0..Nx {
            mat_s[[poly_j, poly_i]] =
                get_quad_weight(lagrange_coeffs, T::zero()..quad_nodes[poly_j]);
        }
    }
}

/// Initialize uniformly spaced quadrature nodes
///
/// # Arguments
/// * quad_nodes    - Mutable slice to store the values
/// * range         - domain where nodes are located
pub fn init_unif_nodes<T>(quad_nodes: &mut [T], ab: std::ops::Range<T>)
where
    T: num_traits::float::Float + num_traits::FromPrimitive,
{
    for (x, y) in itertools::zip(
        itertools_num::linspace(ab.start, ab.end, quad_nodes.len()),
        quad_nodes.iter_mut(),
    ) {
        *y = x;
    }
}

pub fn filter_init(max_order: usize, start_num: usize) -> Array2<bool> {
    let mut filter = Array::from_elem((max_order, start_num), false);

    if max_order == 1 {
        // Special case
        filter[[0, 0]] = true;
    } else {
        let mut q = 0;

        for p in 1..max_order {
            // all except level p step
            for pp in 0..p {
                filter[[pp, q]] = true;
            }

            q += 1;

            // one step
            for _count in 0..(p - 1) {
                filter[[p, q]] = true;
                q += 1;
            }

            // all step, including level p
            for pp in 0..=p {
                filter[[pp, q]] = true;
            }
            q += 1;
        } // loop over order
    } // create filter

    return filter;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_integration_matrices2() {
        let mut mat_s = Array2::<f64>::zeros((3, 3));
        integration_matrices(3 as usize, mat_s.view_mut());
        dbg!(&mat_s);
    }

    /*
    #[test]
    fn test_integration_matrices() {
        let order = 4;
        // initialize integration matrices
        //for j in 1..order { //}
        let mat_s = integration_matrices(order);
        dbg!(mat_s);
        let mat_s = integration_matrices(order - 1);
        dbg!(mat_s);
        let mat_s = integration_matrices(order - 2);
        dbg!(mat_s);

        let mat_s = array![
            [0.0, 0.0, 0.0, 0.0, 0.0],
            [
                0.08715277777777782,
                0.22430555555555556,
                -0.09166666666666669,
                0.036805555555555564,
                -0.0065972222222222265
            ],
            [
                0.08055555555555573,
                0.3444444444444445,
                0.06666666666666654,
                0.011111111111111127,
                -0.0027777777777778095
            ],
            [
                0.08437500000000042,
                0.31875000000000187,
                0.2250000000000001,
                0.1312500000000001,
                -0.009375000000000022
            ],
            [
                0.07777777777777883,
                0.35555555555555607,
                0.13333333333333286,
                0.3555555555555543,
                0.0777777777777775
            ]
        ];
    }
    */
}
