use crate::{util, window_pair::AsWindowPairMut, Corrector, Steppable};
use ndarray::{prelude::*, Slice};
use rayon::iter::{IntoParallelRefMutIterator, ParallelIterator};
use std::{fmt::Debug, marker::PhantomData};
use tracing::{event, instrument, span};

#[derive(Debug)]
struct Level {
    /// The order of this level (index)
    pub order: usize,
    /// Size of memory stencil for this level.
    pub stencil_size: usize,
    /// Flag for whether to shift memory in stencil
    pub do_mem_shift: bool,
    /// Flag for whether the node is able to compute the next step for each correction level
    pub do_compute_step: bool,
    ///
    pub filter: Array1<bool>,
    /// counter for where the node is for each correction level
    pub ii: usize,
    /// Memory for quadrature approximations
    pub u: Array2<f64>,
    /// memory for quadrature approximations, in this case previous level
    pub f_prev: Array2<f64>,
    /// temp for storing the solution at the new time step
    pub u_new: Array1<f64>,
    /// temp for storing the solution at the new time step
    pub f_new: Array1<f64>,
    /// Integration matrix for this level
    pub mat_s: Array2<f64>,
}

impl Level {
    fn new(
        order: usize,
        max_order: usize,
        u0: ArrayView1<f64>,
        f0: ArrayView1<f64>,
        filter: Array1<bool>,
    ) -> Self {
        let stencil_size = if order < (max_order + 1) {
            order + 2
        } else {
            1
        };

        // Use (order + 1) here because it's for the *previous* level
        let f_prev_size = if order == 0 { 0 } else { order + 1 };

        Self {
            order,
            stencil_size,
            do_mem_shift: false,
            do_compute_step: false,
            filter,
            ii: 0,
            u: u0
                .broadcast((stencil_size, u0.len()))
                .expect("Unable to broadcast u0")
                .to_owned(),
            f_prev: f0
                .broadcast((f_prev_size, f0.len()))
                .expect("Unable to broadcast f0")
                .to_owned(),
            u_new: ArrayBase::zeros(u0.len()),
            f_new: ArrayBase::zeros(u0.len()),
            mat_s: util::old::integration_matrices(order + 1),
        }
    }
}

#[derive(Debug)]
pub struct Solver<ODE: Steppable + Sync, const ORDER: usize> {
    /// After start_num steps, all the nodes should be computing.
    start_num: usize,

    levels: Vec<Level>,

    //levels2: [Level; ORDER],
    ti: f64,

    _phantom: PhantomData<ODE>,
}

impl<Ode: Steppable + Sync + Debug, const ORDER: usize> Solver<Ode, ORDER> {
    /// Creates a new Ridc, initializes state
    #[instrument]
    pub fn new(u0: ArrayView1<f64>, ti: f64) -> Self {
        let start_num = (ORDER * (ORDER + 1) / 2 - 1).max(1);
        let filter = util::filter_init(ORDER, start_num);

        // Calculate f0 from u0
        let mut f0 = u0.to_owned();
        Ode::rhs(ti, u0.view(), f0.view_mut());

        Self {
            start_num,
            levels: (0..ORDER)
                .map(|order| Level::new(order, ORDER, u0, f0.view(), filter.row(order).to_owned()))
                .collect(),
            ti,
            _phantom: PhantomData::default(),
        }
    }

    /// integrates solution from ti to tf by bootstrapping the step function.
    #[instrument(level = "trace")]
    pub fn solve<Cor: Corrector>(&mut self, dt: f64, nt: usize) -> ArrayView1<f64> {
        let mut i: usize = 0;

        while self.levels[ORDER - 1].ii < nt {
            let outer_span = span!(tracing::Level::TRACE, "Step i={}", i);
            let _outer_span_guard = outer_span.enter();

            let start_num = &self.start_num;
            let ti = &self.ti;

            self.levels.par_iter_mut().for_each(|lvl: &mut Level| {
                let loop_span = span!(
                    tracing::Level::TRACE,
                    "Main loop order={}, ii={}",
                    lvl.order,
                    lvl.ii
                );
                let _loop_guard = loop_span.enter();

                lvl.do_mem_shift = false;
                lvl.do_compute_step = false;

                if (lvl.ii < nt) && ((i >= (start_num - 1)) || lvl.filter[i]) {
                    lvl.do_compute_step = true;
                }

                if lvl.do_compute_step {
                    // Now do a step of the pth Corrector/Predictor
                    let index_into_array = lvl.ii.min(lvl.stencil_size - 1);

                    // Compute current time
                    let t = ti + ((lvl.ii as f64) * dt);

                    if lvl.order == 0 {
                        // prediction step
                        Ode::step(
                            t,
                            dt,
                            lvl.u.index_axis(Axis(0), index_into_array),
                            lvl.u_new.view_mut(),
                        );

                        // populate memory stencil for f (needed for order > 1)
                        Ode::rhs(t + dt, lvl.u_new.view(), lvl.f_new.view_mut());
                    } else {
                        let index_sth = lvl.ii.min(lvl.order - 1);

                        Cor::correction_step::<Ode>(
                            lvl.u.index_axis(Axis(0), index_into_array), // soln at time level t
                            lvl.f_prev.view(),                           // f[p - 1],
                            lvl.mat_s.view(),
                            index_sth,
                            lvl.order + 1,
                            t,
                            dt,
                            lvl.u_new.view_mut(),
                        );

                        // populate memory stencil if not the last correction loop
                        if lvl.order < (ORDER - 1) {
                            Ode::rhs(t + dt, lvl.u_new.view(), lvl.f_new.view_mut());
                        }
                    }
                    lvl.ii += 1;

                    if lvl.ii >= lvl.stencil_size {
                        lvl.do_mem_shift = true;
                    }
                }
            });

            // memshifts
            self.levels.par_iter_mut().for_each(|lvl: &mut Level| {
                let memshift_span = span!(tracing::Level::TRACE, "MemShiftA order={}", lvl.order);
                let _memshift_guard = memshift_span.enter();
                if lvl.do_compute_step {
                    if !lvl.do_mem_shift {
                        // Place u_new into correct spot in u
                        lvl
                            .u
                            .index_axis_mut(Axis(0), lvl.ii)
                            .assign(&lvl.u_new);
                    } else {
                        // Shuffle all states in u downwards
                        let temp_u = lvl.u.slice_axis(Axis(0), Slice::from(1..)).to_owned();
                        lvl
                            .u
                            .slice_axis_mut(Axis(0), Slice::from(..-1))
                            .assign(&temp_u);
                        // Assign u_new at the end
                        lvl
                            .u
                            .slice_axis_mut(Axis(0), Slice::from(-1..))
                            .assign(&lvl.u_new);
                    }
                }
            });

            let mut level_pairs = self.levels.windows_pair_mut();
            while let Some((level_p0, level_p1)) = level_pairs.next() {
                let memshift_span = span!(tracing::Level::TRACE, "MemShiftB order={}", level_p0.order);
                let _memshift_guard = memshift_span.enter();
                if level_p0.do_compute_step {
                    if !level_p0.do_mem_shift {
                        // Set levels[p+1].f_prev[index] = levels[p].f_new
                        level_p1
                            .f_prev
                            .index_axis_mut(Axis(0), level_p0.ii)
                            .assign(&level_p0.f_new);
                    } else {
                        // Shuffle all states in u_prev downwards
                        let temp_f_prev = level_p1
                            .f_prev
                            .slice_axis(Axis(0), Slice::from(1..))
                            .to_owned();
                        level_p1
                            .f_prev
                            .slice_axis_mut(Axis(0), Slice::from(..-1))
                            .assign(&temp_f_prev);
                        // Assign f_new at the end
                        level_p1
                            .f_prev
                            .slice_axis_mut(Axis(0), Slice::from(-1..))
                            .assign(&level_p0.f_new);
                    }
                }
            }

            if self.levels[ORDER - 1].ii == nt {
                break;
            }
            i += 1;
        } // end while

        let ix = self.levels[ORDER - 1].stencil_size - 1;
        self.levels[ORDER - 1].u.index_axis(Axis(0), ix)
    }
}
