fn main() {
    cc::Build::new()
        .cpp(true)
        .flag("-std=c++17")
        .flag("-Wall")
        .file("cpp/brusselator.cpp")
        .file("cpp/corrector.cpp")
        .file("cpp/ridc.cpp")
        .file("cpp/util.cpp")
        .compile("ridc");
    
    println!("cargo:rustc-link-lib=gsl");
    println!("cargo:rustc-link-lib=gslcblas");
}
