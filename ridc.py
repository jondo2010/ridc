from numpy import *

neq = 2
order = 4
stencilSize = array([2,3,4,1])

# after startnum, all the nodes should be computing
startnum = int(order * (order + 1) / 2 - 1)
filter = zeros((order, startnum))
filter = array([[1., 1., 1., 0., 1., 1., 0., 0., 1.],
                [0., 1., 1., 0., 1., 1., 0., 0., 1.],
                [0., 0., 0., 1., 1., 1., 0., 0., 1.],
                [0., 0., 0., 0., 0., 0., 1., 1., 1.]])

u = array((order, stencilSize, neq))
f = array((order, stencilSize, neq))

u0 = zeros((2,neq))
u1 = zeros((3,neq))
u2 = zeros((4,neq))
u4 = zeros((1,neq))

S1 = array([[ 0. ,  0. ],
            [ 0.5,  0.5]])

S2 = array([[ 0.      ,  0.      ,  0.      ],
            [ 0.208333,  0.333333, -0.041667],
            [ 0.166667,  0.666667,  0.166667]])

S3 = array([[ 0.      ,  0.      ,  0.      ,  0.      ],
            [ 0.125   ,  0.263889, -0.069444,  0.013889],
            [ 0.111111,  0.444444,  0.111111, -0.      ],
            [ 0.125   ,  0.375   ,  0.375   ,  0.125   ]])
