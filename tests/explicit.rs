use ridc::Steppable;

#[derive(Debug)]
pub struct Explicit;
impl Steppable for Explicit {
    fn rhs(t: f64, u: ndarray::ArrayView1<f64>, mut f: ndarray::ArrayViewMut1<f64>) {
        ndarray::Zip::indexed(&mut f).and(&u).apply(|i, f, &u| {
            *f = -(i as f64 + 1.0) * t * u;
        });
    }

    fn step(t: f64, dt: f64, u: ndarray::ArrayView1<f64>, mut u_new: ndarray::ArrayViewMut1<f64>) {
        let mut f_old = ndarray::Array1::zeros(u.raw_dim());
        Self::rhs(t, u, f_old.view_mut());
        ndarray::Zip::from(&mut u_new)
            .and(u)
            .and(&f_old)
            .apply(|u_new, u, f_old| {
                *u_new = u + dt * f_old;
            });
    }
}

unsafe extern "C" fn rhs_explicit(t: f64, u: *const f64, f: *mut f64, neq: libc::size_t) {
    let u = ndarray::ArrayView1::from_shape_ptr(neq, u);
    let f = ndarray::ArrayViewMut1::from_shape_ptr(neq, f);
    Explicit::rhs(t, u, f);
}

unsafe extern "C" fn step_explicit(
    t: f64,
    dt: f64,
    u: *mut f64,
    unew: *mut f64,
    neq: libc::size_t,
) {
    let u = ndarray::ArrayView1::from_shape_ptr(neq, u);
    let u_new = ndarray::ArrayViewMut1::from_shape_ptr(neq, unew);
    Explicit::step(t, dt, u, u_new);
}

const ORDER: usize = 4;
const NEQ: usize = 2;
const TI: f64 = 0.0;
const TF: f64 = 1.0;
const TEST_DATA: [(usize, [f64; NEQ]); 5] = [
    (10, [0.606521722539, 0.367864508325]),
    (20, [0.606530088876, 0.367878543757]),
    (40, [0.606530623802, 0.367879386301]),
    (80, [0.606530657463, 0.367879437782]),
    (160, [0.606530659572, 0.367879440961]),
];

#[test]
pub fn test_cpp() {
    let mut callbacks = ridc::cpp::OdeCallbacks {
        rhs_cb: Some(rhs_explicit),
        step_cb: Some(step_explicit),
    };

    for (nt, expected) in &TEST_DATA[0..5] {
        let mut sol = ndarray::Array1::ones(NEQ);

        let dt = (TF - TI) / (*nt as f64); // compute dt

        unsafe {
            ridc::cpp::ridc(
                (&mut callbacks) as *mut ridc::cpp::OdeCallbacks,
                Some(ridc::cpp::corrector_forwards),
                dt,
                TI,
                TF,
                NEQ,
                *nt,
                ORDER,
                sol.as_mut_ptr(),
            );
        }

        approx::assert_relative_eq!(
            sol,
            ndarray::ArrayView::from(expected),
            max_relative = 2e-12
        );
        println!("{:?}", sol);
    }
}

#[test]
pub fn test_rust() {
    use ndarray::prelude::*;

    rayon::ThreadPoolBuilder::new()
        .num_threads(4)
        .build_global()
        .unwrap();

    pretty_env_logger::init();

    for (nt, expected) in &TEST_DATA[0..5] {
        let u0 = Array1::ones(NEQ);

        let dt = (TF - TI) / (*nt as f64); // compute dt

        let mut ridc = ridc::Solver::<Explicit, ORDER>::new(u0.view(), 0.0);

        let sol = ridc.solve::<ridc::ForwardEuler>(dt, *nt);

        approx::assert_relative_eq!(sol, ArrayView::from(expected), max_relative = 2e-12);
        println!("{:?}", sol);
    }
}
