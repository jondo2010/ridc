use std::{path::Path, fs::File, io::BufReader, io::BufWriter};

use ridc::Steppable;
use tracing::instrument;

#[derive(Debug)]
struct Implicit;
impl Steppable for Implicit {
    #[instrument]
    fn rhs(t: f64, u: ndarray::ArrayView1<f64>, mut f: ndarray::ArrayViewMut1<f64>) {
        ndarray::Zip::indexed(&mut f).and(&u).apply(|i, f, &u| {
            *f = -(i as f64 + 1.0) * t * u;
        });
    }

    #[instrument]
    fn step(t: f64, dt: f64, u: ndarray::ArrayView1<f64>, mut u_new: ndarray::ArrayViewMut1<f64>) {
        // backward Euler update (simplistic is ODE is linear
        ndarray::Zip::indexed(&mut u_new)
            .and(u)
            .apply(|i, unew, &u| {
                *unew = (1.0) / (1.0 + dt * (t + dt) * (i as f64 + 1.0)) * u;
            });
    }
}

unsafe extern "C" fn rhs_implicit(t: f64, u: *const f64, f: *mut f64, neq: libc::size_t) {
    let u = ndarray::ArrayView1::from_shape_ptr(neq, u);
    let f = ndarray::ArrayViewMut1::from_shape_ptr(neq, f);
    Implicit::rhs(t, u, f);
}

unsafe extern "C" fn step_implicit(
    t: f64,
    dt: f64,
    u: *mut f64,
    unew: *mut f64,
    neq: libc::size_t,
) {
    let u = ndarray::ArrayView1::from_shape_ptr(neq, u);
    let u_new = ndarray::ArrayViewMut1::from_shape_ptr(neq, unew);
    Implicit::step(t, dt, u, u_new);
}

const ORDER: usize = 4;
const NEQ: usize = 2;
const TI: f64 = 0.0;
const TF: f64 = 1.0;
const TEST_DATA: [(usize, [f64; NEQ]); 5] = [
    (10, [0.606523928823, 0.367857164648]),
    (20, [0.606530218112, 0.367878080515]),
    (40, [0.606530631588, 0.367879358032]),
    (80, [0.606530657940, 0.367879436048]),
    (160, [0.606530659601, 0.367879440854]),
];

#[test]
pub fn test_implicit() {
    let mut callbacks = ridc::cpp::OdeCallbacks {
        rhs_cb: Some(rhs_implicit),
        step_cb: Some(step_implicit),
    };

    for (nt, expected) in &TEST_DATA[0..5] {
        let mut sol = ndarray::Array1::ones(NEQ);

        let dt = (TF - TI) / (*nt as f64); // compute dt

        unsafe {
            ridc::cpp::ridc(
                (&mut callbacks) as *mut ridc::cpp::OdeCallbacks,
                Some(ridc::cpp::corrector_backwards),
                dt,
                TI,
                TF,
                NEQ,
                *nt,
                ORDER,
                sol.as_mut_ptr(),
            );
        }

        approx::assert_relative_eq!(
            sol,
            ndarray::ArrayView::from(expected),
            max_relative = 1e-12
        );
        println!("{:?}", sol);
    }
}

#[test]
pub fn test_rust() {
    use ndarray::prelude::*;

    rayon::ThreadPoolBuilder::new()
        .num_threads(4)
        .build_global()
        .unwrap();

    pretty_env_logger::init();

    for (nt, expected) in &TEST_DATA[0..5] {
        let u0 = Array1::ones(NEQ);

        let dt = (TF - TI) / (*nt as f64); // compute dt

        let mut ridc = ridc::Solver::<Implicit, ORDER>::new(u0.view(), TI);

        let sol = ridc.solve::<ridc::BackwardEuler>(dt, *nt);

        approx::assert_relative_eq!(sol, ArrayView::from(expected), max_relative = 2e-12);
        println!("{:?}", sol);
    }
}

static PATH: &str = "flame.folded";
fn setup_global_subscriber(dir: &Path) -> impl Drop {
    use tracing_subscriber::{prelude::*, registry::Registry};

    //let fmt_layer = tracing_subscriber::fmt::Layer::default();
    let (flame_layer, _guard) = tracing_flame::FlameLayer::with_file(dir.join(PATH)).unwrap();
    //let (chrome_layer, _guard) = tracing_chrome::ChromeLayerBuilder::new().build();

    let subscriber = Registry::default()
        //.with(fmt_layer)
        .with(flame_layer)
        //.with(chrome_layer)
        ;

    tracing::subscriber::set_global_default(subscriber).expect("Could not set global default");
    _guard
}

fn make_flamegraph(dir: &Path) {
    let inf = File::open(dir.join(PATH)).unwrap();
    let reader = BufReader::new(inf);

    let out = File::create(dir.join("inferno.svg")).unwrap();
    let writer = BufWriter::new(out);

    let mut opts = inferno::flamegraph::Options::default();
    inferno::flamegraph::from_reader(&mut opts, reader, writer).unwrap();
}

#[test]
pub fn test_big() {
    use ndarray::prelude::*;
    use tempdir::TempDir;

    rayon::ThreadPoolBuilder::new()
        .num_threads(4)
        .build_global()
        .unwrap();

    //let tmp_dir = TempDir::new("flamegraphs").unwrap();
    let flamegraph_path = Path::new("flamegraphs");
    let guard = setup_global_subscriber(flamegraph_path);

    const ORDER: usize = 6;
    const NEQ: usize = 1000;
    const NT: usize = 1000;
    let dt = (10.0 - TI) / (NT as f64); // compute dt
    let u0 = Array1::ones(NEQ);
    let mut ridc = ridc::Solver::<Implicit, ORDER>::new(u0.view(), TI);
    let sol = ridc.solve::<ridc::BackwardEuler>(dt, NT);

    println!("{:?}", sol);

    drop(guard);
    make_flamegraph(flamegraph_path);
}
