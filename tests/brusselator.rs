use std::ops::Neg;

use ndarray::prelude::*;
use ndarray_linalg::{Factorize, Inverse, Solve};
use ridc::Steppable;

/// These are implemented in brusselator.cpp
//#[link(name = "gsl", kind = "dylib")]
extern "C" {
    //fn bruss_rhs(t: f64, u: *const f64, f: *mut f64, neq: libc::size_t);
    fn bruss_step(t: f64, dt: f64, u: *mut f64, unew: *mut f64, neq: libc::size_t);
    fn bruss_jac(t: f64, dt: f64, u: *mut f64, J: *mut f64, neq: libc::size_t);
}

/// Rust impl of the Brusselator fns
pub struct Brusselator;
impl Steppable for Brusselator {
    fn rhs(_t: f64, u: ArrayView1<f64>, mut f: ArrayViewMut1<f64>) {
        const A: f64 = 1.0;
        const B: f64 = 3.0;
        const ALPHA: f64 = 0.02;
        let neq = u.len();
        let dx = 1.0 / (neq + 1) as f64;
        let nx = neq / 2;

        f[0] = A + u[0] * u[0] * u[nx] - (B + 1.0) * u[0]
            + ALPHA / dx / dx * (u[1] - 2.0 * u[0] + 1.0);
        f[nx - 1] = A + u[nx - 1] * u[nx - 1] * u[2 * nx - 1] - (B + 1.0) * u[nx - 1]
            + ALPHA / dx / dx * (1.0 - 2.0 * u[nx - 1] + u[nx - 2]);

        f[nx] = B * u[0] - u[0] * u[0] * u[nx] + ALPHA / dx / dx * (u[nx + 1] - 2.0 * u[nx] + 3.0);
        f[2 * nx - 1] = B * u[nx - 1] - u[nx - 1] * u[nx - 1] * u[2 * nx - 1]
            + ALPHA / dx / dx * (3.0 - 2.0 * u[2 * nx - 1] + u[2 * nx - 2]);

        for i in 1..nx - 1 {
            f[i] = A + u[i] * u[i] * u[nx + i] - (B + 1.0) * u[i]
                + ALPHA / dx / dx * (u[i + 1] - 2.0 * u[i] + u[i - 1]);
            f[nx + i] = B * u[i] - u[i] * u[i] * u[nx + i]
                + ALPHA / dx / dx * (u[nx + i + 1] - 2.0 * u[nx + i] + u[nx + i - 1]);
        }
    }
    fn step(t: f64, dt: f64, u: ArrayView1<f64>, mut u_new: ArrayViewMut1<f64>) {
        const NEWTON_TOL: f64 = 1.0e-14;
        const NEWTON_MAXSTEP: usize = 1000;

        let tnew = t + dt;

        // how far from zero we are
        let mut stepsize = Array1::zeros(u.raw_dim());
        // current solution guess
        u_new.assign(&u);

        let mut mat_j = Array2::zeros((u.len(), u.len()));

        let mut counter = 0;
        loop {
            // Compute the next Newton step for solving a system of equations "bruss_newt"
            Self::rhs(tnew, u_new.view(), stepsize.view_mut());
            ndarray::Zip::from(&mut stepsize).and(&u_new).and(&u).apply(
                |stepsize, &u_guess, &u| {
                    *stepsize = u_guess - u - (*stepsize * dt);
                },
            );

            // compute numerical Jacobian
            Self::jac(tnew, dt, u_new.view(), mat_j.view_mut());
            mat_j -= &ndarray::Array2::eye(u.len());

            let x = mat_j.solve(&u_new.neg()).unwrap();

            // Check for convergence
            u_new += &x;
            let max_stepsize =
                ndarray::Zip::from(&stepsize)
                    .and(&x)
                    .fold(0.0, |max_stepsize, stepsize, x| {
                        if stepsize.abs() > max_stepsize {
                            x.abs()
                        } else {
                            max_stepsize
                        }
                    });

            // if update sufficiently small enough
            if max_stepsize < NEWTON_TOL {
                break;
            }

            counter += 1;
            //error if too many steps
            if counter > NEWTON_MAXSTEP {
                panic!("Max Newton iterations reached.");
            }
        }
    }
}

unsafe extern "C" fn rhs(t: f64, u: *const f64, f: *mut f64, neq: libc::size_t) {
    let u = ndarray::ArrayView1::from_shape_ptr(neq, u);
    let f = ndarray::ArrayViewMut1::from_shape_ptr(neq, f);
    Brusselator::rhs(t, u, f);
}

unsafe extern "C" fn step(t: f64, dt: f64, u: *mut f64, unew: *mut f64, neq: libc::size_t) {
    let u = ndarray::ArrayView1::from_shape_ptr(neq, u);
    let u_new = ndarray::ArrayViewMut1::from_shape_ptr(neq, unew);
    Brusselator::step(t, dt, u, u_new);
}

#[test]
pub fn test_brusselator() {
    use ndarray::prelude::*;

    const ORDER: usize = 4;
    const NEQ: usize = 100;

    let mut callbacks = ridc::cpp::OdeCallbacks {
        rhs_cb: Some(rhs),
        step_cb: Some(step),
    };

    let steps = array![100, 200, 400, 800, 1600];
    let test_data: ndarray::Array2<f64> =
        ndarray_npy::read_npy("tests/brusselator_ref.npy").unwrap();

    let mut sol = ndarray::Array1::zeros(NEQ);

    ndarray::Zip::from(&steps)
        .and(test_data.genrows())
        .apply(|&nt, expected| {
            let ti = 0.0;
            let tf = 10.0;
            let dt = (tf - ti) / nt as f64; // compute dt

            // Calculate ICs
            let nx = NEQ / 2;
            let dx = 1.0 / (nx + 1) as f64;

            for (i, sol) in sol.slice_mut(s![0..nx]).indexed_iter_mut() {
                let xi = (i as f64 + 1.0) * dx;
                *sol = 1.0 + (2.0 * std::f64::consts::PI * xi).sin();
            }
            sol.slice_mut(s![nx..]).fill(3.);

            unsafe {
                println!("running brusselator order {} ridc with nt = {}", ORDER, nt);
                ridc::cpp::ridc(
                    (&mut callbacks) as *mut ridc::cpp::OdeCallbacks,
                    Some(ridc::cpp::corr_be),
                    dt,
                    ti,
                    tf,
                    NEQ,
                    nt,
                    ORDER,
                    sol.as_mut_ptr(),
                );
            }

            //println!("{:#?}", sol);
            approx::assert_relative_eq!(
                sol,
                ndarray::ArrayView::from(expected),
                max_relative = 1e-12
            );
        });
}

struct Test;
impl Steppable for Test {
    const D: f64 = 1e-5;

    fn rhs(t: f64, u: ArrayView1<f64>, mut f: ArrayViewMut1<f64>) {
        f[0] = u[0] * u[0] * u[1];
        f[1] = 5.0 * u[0] + u[1].sin();
        f[2] = u[0];
    }

    fn step(t: f64, dt: f64, u: ArrayView1<f64>, u_new: ArrayViewMut1<f64>) {
        todo!()
    }
}

#[test]
fn test_jac() {
    use ndarray_linalg::Determinant;
    let u: Array1<f64> = array![2.0, 3.0, 4.0];
    let analytic_jac = array![
        [2.0 * u[0] * u[1], u[0] * u[0], 0.0],
        [5.0, u[1].cos(), 0.0],
        [1.0, 0.0, 0.0],
    ];

    let mut num_jac = Array2::zeros((3, 3));
    Test::jac(0.0, 1.0, u.view(), num_jac.view_mut());

    let res = &num_jac - &analytic_jac;
    println!("{:#?}", res.det());

    num_jac *= -1.0;
    num_jac += &ndarray::Array::eye(3);
    let inv = num_jac.inv();
    dbg!(&inv);
}

#[test]
fn test_brus2() {
    const NEQ: usize = 4;
    let mut sol = ndarray::Array1::zeros(NEQ);
    let nx = NEQ / 2;
    let dx = 1.0 / (nx + 1) as f64;
    let dt = 0.1;

    for (i, sol) in sol.slice_mut(s![0..nx]).indexed_iter_mut() {
        let xi = (i as f64 + 1.0) * dx;
        *sol = 1.0 + (2.0 * std::f64::consts::PI * xi).sin();
    }
    sol.slice_mut(s![nx..]).fill(3.);

    let mut jac = Array2::zeros((4, 4));
    Brusselator::jac(0.0, dt, sol.view(), jac.view_mut());
    jac -= &Array::eye(4);
    println!("{:#?}", &jac);

    let sol_neg = &sol * -1.0;
    let x = jac.solve(&sol_neg);
    dbg!(&x);

    unsafe {
        bruss_jac(0.0, dt, sol.as_mut_ptr(), jac.as_mut_ptr(), 4);
    }
    println!("{:#?}", &jac);
}
